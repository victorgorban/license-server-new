import _ from "lodash";
import Template from "./mongoTemplate";
import SimpleSchema, { Any, Integer, oneOf } from "simpl-schema";
import * as serverHelpers from "@serverHelpers";

export let collectionSchema = new SimpleSchema(
  {
    _id: String,
    createdAt: Date,
    updatedAt: Date,
    fullName: String,
    shortName: String,
    email: String,
    isArchived: Boolean,
    archivedAt: Date
    // здесь должны быть и другие поля, но для целей простейшей лицензии это не нужно
    // код привязывается к машине на которой приложение активируется. При переносе на другую машину нужно будет перепривязать.
    // или можно с этим не париться и жестко привязывать к ID машины + адресу лицензии, а привязка лицензии к новому ID - только вручную.
    // Получить MAC можно через networkInterfaces, я это делал на Electron. Или systeminformation.system() - UUID и serial. Там же есть bios, baseboard
    // на email и/или тел отправляются коды подтверждения при активации кода на компьютере. 
    // для организации контрольные email и телефон это email и телефон организации.
  },
  serverHelpers.simpleSchemaOptions
);


export default class CollectionClass extends Template {
  static collectionName = "companies";

  static indexes = [
    // {
    //   fieldName: "username",
    //   unique: true,
    //   sparse: true,
    // },
  ];

  static schema = collectionSchema;

  // TODO очевидно здесь требуется триггер на архивацию хотя бы и удаления. Удаление есть в deleteOldData. 
  // Блин, если исходить из того что изменение данных должно отображаться в лиц. ключе, то вообще любое изменение тогда должно... 
  // Но нет, важны только чувствительные данные. Про архивацию и удаление нужно предупредить получше просто - нужен кастомный текст в тех модалках.

  static async afterUpdateOne(updatedDoc, updateQuery) {
    console.log('companies afterUpdateOne', updatedDoc, updateQuery)
    if (_.has(updateQuery, '$set.shortName') || _.has(updateQuery, '$set.fullName')) {
      let shortName = _.get(updateQuery, '$set.shortName')
      let fullName = _.get(updateQuery, '$set.fullName')
      await global.db.collection('licenseKeys').updateMany({ companyId: updatedDoc._id }, { $set: { companyData: { shortName, fullName } } })
    }
  }
}
