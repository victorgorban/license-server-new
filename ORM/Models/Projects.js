import _ from "lodash";
import Template from "./mongoTemplate";
import SimpleSchema, { Any, Integer, oneOf } from "simpl-schema";
import * as serverHelpers from "@serverHelpers";

export let collectionSchema = new SimpleSchema(
  {
    _id: String,
    createdAt: Date,
    updatedAt: Date,
    name: String,
    appId: String,
    isArchived: Boolean,
    archivedAt: Date
  },
  serverHelpers.simpleSchemaOptions
);


export default class CollectionClass extends Template {
  static collectionName = "projects";

  static indexes = [

  ];

  static schema = collectionSchema;

  static async afterUpdateOne(updatedDoc, updateQuery) {
    console.log('projects afterUpdateOne', updatedDoc, updateQuery)
    if (_.has(updateQuery, '$set.appId')) {
      let appId = _.get(updateQuery, '$set.appId')
      await global.db.collection('licenseKeys').updateMany({ projectId: updatedDoc._id }, { $set: { projectAppId: appId } })
    }
  }
}
