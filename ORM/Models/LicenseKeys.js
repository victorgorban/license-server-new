import _ from "lodash";
import Template from "./mongoTemplate";
import Oplog from "./Oplog"
import SimpleSchema, { Any, Integer, oneOf } from "simpl-schema";
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";

export let deviceInfoSchema = new SimpleSchema({ system: String, cpu: String }, serverHelpers.simpleSchemaOptions)


/**
 * Заметка по поводу нормализации.
 * Эта коллекция, лицензионные ключи, является собранием всей информации в этой мини-системе. Все остальные коллекции являются лишь вспомогательными.
 * Поля здесь (типа companyData) обновляются при изменении их в родительском объекте, по самописным триггерам. 
 * Почему триггеры самописные, а не встроенные? Потому что Mongo имеет триггеры (и полноценные join-ы) только в MongoDB Atlas, т.е. в облачном решении.
 * При всем уважении к работе Atlas и всему удобству использования, все же я предпочитаю свой сервер/кластер серверов:
 * Это полное управление, это полное понимание "что происходит", это возможность изолировать весь проект в условном контейнере.
 * 
 * Так вот, полноценной нормализации именно здесь нет не только потому что это неудобно, но в первую очередь по 2м причинам:
 * 1. История изменений: т.к. информация о проекте и привязанном юзере/компании тесно привязана к ключу 
 * и всегда отображается вместе с ним, а также на основе привязанных данных этот самый ключ активируется,
 * изменение связанного пользователя означает изменение ключа и почти всегда его инвалидацию.
 * 2. Отмена изменений: история изменений сама по себе в принципе нужна только для того, 
 * чтобы защитить от невозвратных потерь при человеческой ошибке, а также упростить поиск проблемы и ее решение.
 * Человек (админ, ну или ошибка при вызове rest api) с тем же успехом может изменить email 
 * (может телефон, в зависимости от реализации проверки) в юзере, и это инвалидирует ключ.
 * А потом он нам пишет "что случилось, чего не работает? Вот неделю назад работало". 
 * И мы именно по истории ключа видим, что изменилось неделю назад. И возвращая ключ к нужной версии, мы исправляем все проблемы, а не часть.
 * Без этой логики "всё связанное в одном месте" администратору будет очень тяжело как выяснять возможную проблему, 
 * так и решать ее корректно, чтобы ничего не забыть. 
 */

export let collectionSchema = new SimpleSchema(
  {
    _id: String,
    createdAt: Date,
    updatedAt: Date,
    /** объект может быть временным, если он создан для просмотра и применения истории изменений */
    isTemporary: Boolean,
    projectId: String,
    projectAppId: String,
    companyId: String,
    userId: String,
    keyString: String,
    deactivationMethod: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.deactivationMethodsForSelect.map(obj => obj.value),
    },
    activationScope: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.activationScopesForSelect.map(obj => obj.value),
    },
    protectionType: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.protectionTypesForSelect.map(obj => obj.value),
      defaultValue: 'none'
    },
    protectionSettings: {
      type: Object,
      blackbox: true
    },
    transmissionProtectionAesKey: {
      type: Array,
    },
    'transmissionProtectionAesKey.$': Number,
    deviceValidPeriod: Number,
    maxDevices: Number,
    devices: {
      type: Array,
      defaultValue: []
    },
    'devices.$': Object,
    'devices.$.signature': String,
    'devices.$.activationDate': Date,
    // заполняется только для deactivationMethod=='perDevice'
    'devices.$.validUntil': Date,
    'devices.$.deviceInfo': { type: deviceInfoSchema, required: true },
    validUntil: Date,
    // статус - для ключей без срока действия
    status: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.statusesForSelect.map(obj => obj.value),
      defaultValue: 'active'
    },
    isArchived: Boolean,
    archivedAt: Date
    // здесь должны быть и другие поля, но для целей простейшей лицензии это не нужно
  },
  serverHelpers.simpleSchemaOptions
);


export default class CollectionClass extends Template {
  static collectionName = "licenseKeys";

  static indexes = [
    {
      definition: { keyString: 1 },
      options: {
        unique: true,
      },
    },
  ];

  static schema = collectionSchema;

  // TODO replaceOne, как минимум для отката истории. ReplaceOne только для отката, updateOne для нормальных процессов.

  static async afterDeleteOne(deletedDoc) {
    let oplogData = {
      collectionName: this.collectionName,
      method: 'deleteOne',
      findQuery: { _id: deletedDoc._id },
      stateBefore: deletedDoc
    }
    await Oplog.insertOne(oplogData);
  }

  static async afterReplaceOne(replaceDoc, stateBefore) {
    let oplogData = {
      collectionName: this.collectionName,
      method: 'replaceOne',
      findQuery: { _id: replaceDoc._id },
      actionQuery: replaceDoc,
      stateBefore: stateBefore
    }
    await Oplog.insertOne(oplogData);
  }

  static async afterArchiveOne(archivedDoc, updateQuery, stateBefore) {
    let oplogData = {
      collectionName: this.collectionName,
      method: 'archiveOne',
      findQuery: { _id: archivedDoc._id },
      actionQuery: updateQuery,
      stateBefore: stateBefore
    }
    await Oplog.insertOne(oplogData);
  }

  static async afterUpdateOne(updatedDoc, updateQuery, stateBefore) {
    let currentUpdateSet = {}
    if (_.has(updateQuery, '$set.activationScope')) {
      let activationScope = _.get(updateQuery, '$set.activationScope')
      switch (activationScope) {
        case 'project':
          _.set(updateQuery, '$set.companyId', null)
          _.set(updateQuery, '$set.companyData', null)
          _.set(updateQuery, '$set.userId', null)
          _.set(updateQuery, '$set.userData', null)
          _.set(currentUpdateSet, 'companyId', null)
          _.set(currentUpdateSet, 'companyData', null)
          _.set(currentUpdateSet, 'userId', null)
          _.set(currentUpdateSet, 'userData', null)
        case 'personal':
          _.set(updateQuery, '$set.companyId', null)
          _.set(updateQuery, '$set.companyData', null)
          _.set(currentUpdateSet, 'companyId', null)
          _.set(currentUpdateSet, 'companyData', null)
        case 'company':
          _.set(updateQuery, '$set.userId', null)
          _.set(updateQuery, '$set.userData', null)
          _.set(currentUpdateSet, 'userId', null)
          _.set(currentUpdateSet, 'userData', null)
        default: break;
      }
    }

    async function checkProject() {
      if (_.has(updateQuery, '$set.projectId')) {
        let projectId = _.get(updateQuery, '$set.projectId')
        if (!projectId) return;
        let project = await global.db.collection('projects').findOne({_id: projectId});
        if (project) {
          currentUpdateSet.projectAppId = project.appId
        }
      }
    }

    await Promise.all([
      checkProject()
    ])

    if (!_.isEqual(currentUpdateSet, {})) {
      await global.db.collection('licenseKeys').updateOne({ _id: updatedDoc._id }, { $set: currentUpdateSet })
    }


    let oplogData = {
      collectionName: this.collectionName,
      method: 'updateOne',
      findQuery: { _id: updatedDoc._id },
      actionQuery: { ...updateQuery, $set: { ...updateQuery.$set, ...currentUpdateSet } },
      stateBefore: stateBefore
    }
    await Oplog.insertOne(oplogData);
  }

  // TODO логика oplog. тут всё делать надо - insertOne, updateOne, archiveOne, deleteOne, и потом еще replaceOne.

  static async afterInsertOne(insertedDoc) {
    let currentUpdateSet = {}

    async function checkProject() {
      if (_.has(insertedDoc, 'projectId')) {
        let projectId = _.get(insertedDoc, 'projectId')
        if (!projectId) return;
        let project = await global.db.collection('projects').findOne({_id: projectId});
        if (project) {
          currentUpdateSet.projectAppId = project.appId
        }
      }
    }

    await Promise.all([
      checkProject()
    ])

    if (!_.isEqual(currentUpdateSet, {})) {
      await global.db.collection('licenseKeys').updateOne({ _id: insertedDoc._id }, { $set: currentUpdateSet })
    }

    let oplogData = {
      // _id: commonHelpers.randomString(),
      // createdAt: new Date(),
      collectionName: this.collectionName,
      method: 'insertOne',
      findQuery: { _id: insertedDoc._id },
      actionQuery: { ...insertedDoc, ...currentUpdateSet },
    }
    await Oplog.insertOne(oplogData);
  }
}

// TODO про просмотр версии и откат версии: просто создать отдельную запись, к которой применить все нужные изменения.
// TODO очевидно что при обновлении мне нужно 2 версии: до и после. "После" - ответ после обновления, "До" очень нужно будет в oplog, т.к. иначе не получить точную версию "до".
// эта версия и так есть или вычисляется у всех методов, кроме update и archive. В archive это в принципе не особо нужно, т.к. изменяемые поля всегда известны и их всего 2.
// то есть только у update.