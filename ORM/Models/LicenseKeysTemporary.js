import _ from "lodash";
import Template from "./mongoTemplate";
import SimpleSchema, { Any, Integer, oneOf } from "simpl-schema";
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";

export let collectionSchema = null


export default class CollectionClass extends Template {
  static collectionName = "licenseKeysTemporary";

  static indexes = [
    {
      definition: { createdAt: 1 },
      options: {
        expireAfterSeconds: 604_800 // 7 дней
      },
    },
  ];

  static schema = collectionSchema;
}

// TODO про просмотр версии и откат версии: просто создать отдельную запись, к которой применить все нужные изменения.
// TODO очевидно что при обновлении мне нужно 2 версии: до и после. "После" - ответ после обновления, "До" очень нужно будет в oplog, т.к. иначе не получить точную версию "до".
// эта версия и так есть или вычисляется у всех методов, кроме update и archive. В archive это в принципе не особо нужно, т.к. изменяемые поля всегда известны и их всего 2.
// то есть только у update.