'use client'
//* секция Библиотеки c функциями
import React from 'react';
import dayjs from "dayjs";
import _ from 'lodash';

//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers'
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import { Controller, useForm } from 'react-hook-form';
//* endof  Компоненты из библиотек

//* секция Наши компоненты
import RoundIcon from '@components/wrappers/RoundIcon'
import Tooltip from '@components/_global/Tooltip'
import * as formComponents from '@components/forms'
//* endof  Наши компоненты

//* секция Стили компонента
//* endof  Стили компонента


export default React.forwardRef(function Component({ isDisabled, disabledFields = [], mode = "create", selectedObject }, elRef) {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения

    //* контекст
    //* endof контекст


    //* состояние
    const [current_formState, setCurrent_formState] = React.useState({});

    const {
        control,
        handleSubmit,
        reset,
        setValue
    } = useForm({
        mode: 'onTouched'
    });
    //* endof состояние

    //* секция вычисляемые переменные, изменение состояния

    //* endof вычисляемые переменные, изменение состояния

    //* эффекты

    // Форма создается и уходит каждый раз при открытии модалки
    React.useEffect(() => {
        performReset(getDefaultValues());
    }, [])

    React.useImperativeHandle(elRef, () => ({
        performReset(newObj) {
            performReset(newObj || getDefaultValues())
        },
        getFormState() {
            return current_formState;
        },
        handleSubmit
    }))

    //* endof эффекты

    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ
    function getDefaultValues() {
        if (!selectedObject) return {}
        // я использую перечисление, потому это удобно (видны все используемые поля). К тому же очень часто поля формы не соответствуют полям конечного объекта.
        let keys = ['email', 'shortName', 'fullName'] // пароль не идет сюда. Ввод значения означает смену
        let result = {}
        for (let key of keys) {
            result[key] = selectedObject[key]
        }

        return result;
    }

    function performReset(newValues) {
        
        setCurrent_formState(_.cloneDeep(newValues))
        reset(_.cloneDeep(newValues));
    }
    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики
    //* endof обработчики

    return (
        <div className="content form d-flex flex-wrap pt-30 pb-40">
            {(() => {
                let fieldName = 'shortName'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: 'Поле обязательно',
                            validate(v) {

                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Краткое название *</span>

                                <div className="input-wrapper">
                                    <formComponents.TextInput
                                        {...field}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                        }}
                                        disabled={isDisabledField}
                                        placeholder="BT LLC."
                                    />
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'fullName'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: 'Поле обязательно',
                            validate(v) {
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Полное название*</span>

                                <div className="input-wrapper">
                                    <formComponents.TextInput
                                        {...field}
                                        disabled={isDisabledField}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                        }}
                                        placeholder="Blockchain Technologies, LLC."
                                    />
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'email'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: 'Поле обязательно',
                            validate(v) {
                                if (v && !commonHelpers.validateEmail(v)) {
                                    return "Email неверного формата"
                                }
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Email *</span>

                                <div className="input-wrapper">
                                    <formComponents.TextInput
                                        {...field}
                                        disabled={isDisabledField}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            if (e.currentTarget.value) { // invalid date в onChange пустая вообще. А с Invalid Date не будет работать добавление дат.
                                                clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                            }
                                        }}
                                    />

                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

        </div>
    )
})