'use client'
//* секция Библиотеки c функциями
import React from 'react';
import dayjs from "dayjs";
import _ from 'lodash';

//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers'
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import { Controller, useForm } from 'react-hook-form';
//* endof  Компоненты из библиотек

//* секция Наши компоненты
import RoundIcon from '@components/wrappers/RoundIcon'
import Tooltip from '@components/_global/Tooltip'
import * as formComponents from '@components/forms'
//* endof  Наши компоненты

//* секция Стили компонента
//* endof  Стили компонента


export default React.forwardRef(function Component({ isDisabled, disabledFields = [], mode="create", loadCompanyById, selectedObject, defaultCompaniesOptions = [], debounceLoadCompaniesOptions }, elRef) {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения

    //* контекст
    //* endof контекст


    //* состояние
    const [selectedCompany, setSelectedCompany] = React.useState()
    const [selectedObjectRole, setSelectedObjectRole] = React.useState()

    const [current_formState, setCurrent_formState] = React.useState({});

    const {
        control,
        handleSubmit,
        reset,
        setValue
    } = useForm({
        mode: 'onTouched'
    });
    //* endof состояние

    //* секция вычисляемые переменные, изменение состояния

    let companiesForSelect = _.cloneDeep(defaultCompaniesOptions);
    if (selectedCompany && !defaultCompaniesOptions.find(option => option.value == selectedCompany._id)) {
        companiesForSelect.unshift({ value: selectedCompany._id, label: commonHelpers.formatCompanyName(selectedCompany) })
    }

    let rolesForSelect = commonHelpers.users.rolesForSelect;
    //* endof вычисляемые переменные, изменение состояния

    //* эффекты

    // Форма создается и уходит каждый раз при открытии модалки
    React.useEffect(() => {
        
        performReset(getDefaultValues());
    }, [])

    React.useImperativeHandle(elRef, () => ({
        performReset(newObj) {
            performReset(newObj || getDefaultValues())
        },
        getFormState() {
            return current_formState;
        },
        handleSubmit
    }))

    //* endof эффекты

    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ
    function getDefaultValues() {
        if (!selectedObject) return {}
        // я использую перечисление, потому это удобно (видны все используемые поля). К тому же очень часто поля формы не соответствуют полям конечного объекта.
        let keys = ['name', 'email', 'phone', 'username', 'companyId', 'role'] // пароль не идет сюда. Ввод значения означает смену
        let result = {}
        for (let key of keys) {
            result[key] = selectedObject[key]
        }

        return result;
    }

    function performReset(newValues) {
        
        setCurrent_formState(_.cloneDeep(newValues))
        setSelectedObjectRole(newValues.role);
        loadCompanyById(newValues.companyId, setSelectedCompany);
        reset(_.cloneDeep(newValues));
    }
    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики
    //* endof обработчики

    return (
        <div className="content form d-flex flex-wrap pt-30 pb-40">
            {(() => {
                let fieldName = 'name'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: 'Поле обязательно',
                            validate(v) {

                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Имя *</span>

                                <div className="input-wrapper">
                                    <formComponents.TextInput
                                        {...field}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                        }}
                                        disabled={isDisabledField}
                                    />
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'username'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: 'Поле обязательно',
                            validate(v) {
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Имя пользователя*</span>

                                <div className="input-wrapper">
                                    <formComponents.TextInput
                                        {...field}
                                        disabled={isDisabledField}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                        }}
                                    />
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'email'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            validate(v) {
                                if (v && !commonHelpers.validateEmail(v)) {
                                    return "Email неверного формата"
                                }
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Email </span>

                                <div className="input-wrapper">
                                    <formComponents.TextInput
                                        {...field}
                                        disabled={isDisabledField}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            if (e.currentTarget.value) { // invalid date в onChange пустая вообще. А с Invalid Date не будет работать добавление дат.
                                                clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                            }
                                        }}
                                    />

                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'phone'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            validate(v) {
                                if (!v) return;
                                let valueNoExtras = v.replace(/_|\+|-|\(|\)/g, '')
                                // if (!valueNoExtras.length) {
                                //     return 'Заполните мобильный телефон'
                                // }
                                if (valueNoExtras.length && valueNoExtras.length < 11) {
                                    return 'Номер неполный'
                                }
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Телефон*</span>

                                <div className="input-wrapper">
                                    <formComponents.MaskedInput
                                        {...field}
                                        disabled={isDisabledField}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                        }}
                                        mask="+7(999)999-99-99"
                                        placeholder="+7(918)111-22-33"
                                        type="tel"
                                    />
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'password'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return mode == 'edit' ? <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            validate(v) {
                                if (v && v.length < 3) {
                                    return 'Пароль должен содержать минимум 3 символа';
                                }
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Пароль</span>

                                <div className="input-wrapper">
                                    <formComponents.TextInput
                                        {...field}
                                        disabled={isDisabledField}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                        }}
                                        placeholder="Введите чтобы сменить"
                                    />
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </> : <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: 'Поле обязательно',
                            validate(v) {
                                if (v && v.length < 3) {
                                    return 'Пароль должен содержать минимум 3 символа';
                                }
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Пароль*</span>

                                <div className="input-wrapper">
                                    <formComponents.TextInput
                                        {...field}
                                        disabled={isDisabledField}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                        }}
                                    />
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'role'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: "Поле обязательно",
                            validate(v) {
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Роль*</span>

                                <div className="d-flex justify-content-between w-100">
                                    <div className="input-wrapper" style={{ width: 'calc(100% - 40px)' }}>
                                        <formComponents.SelectInput
                                            {...field}
                                            isDisabled={isDisabledField}
                                            value={rolesForSelect.find(sfs => sfs.value == selectedObjectRole)}
                                            onChange={(option, config) => {
                                                field.onChange(option.value);
                                                setSelectedObjectRole(option.value)
                                                clientHelpers.handleChangeItemSelect(option, current_formState, fieldName);
                                            }}
                                            noOptionsMessage={() => 'Ничего не найдено...'}
                                            loadOptions={(inputValue, callback) => {
                                                callback(rolesForSelect.filter(x => x.label.toLowerCase().includes(inputValue.toLowerCase())).slice(0, 50))
                                            }}
                                            defaultOptions={rolesForSelect.slice(0, 50)}
                                            isSearchable={rolesForSelect.length > 7}
                                        />
                                    </div>

                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {selectedObjectRole == 'companyAdmin' ?
                (() => {
                    let fieldName = 'companyId'
                    let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                    return <>
                        <Controller
                            control={control}
                            name={fieldName}
                            rules={{
                                required: 'Укажите компанию',
                                validate(v) {
                                    return true;
                                }
                            }}
                            render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                                <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                    <span className="label weight-600 size-14">Компания *</span>

                                    <formComponents.SelectInput
                                        onChange={(option, config) => {
                                            field.onChange(option.value);
                                            loadCompanyById(option.value, setSelectedCompany);
                                            clientHelpers.handleChangeItemSelect(option, current_formState, fieldName);
                                        }}
                                        isDisabled={isDisabledField}
                                        noOptionsMessage={() => 'Ничего не найдено...'}
                                        value={companiesForSelect.find(sfs => sfs.value == field.value)}
                                        loadOptions={debounceLoadCompaniesOptions}
                                        defaultOptions={companiesForSelect}
                                        isSearchable={companiesForSelect.length > 7}
                                    />

                                    <span className="field-error">{error?.message}</span>
                                </label>
                            )}
                        />
                    </>
                })()
                : null}
        </div>
    )
})