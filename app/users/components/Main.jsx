//* секция Библиотеки c функциями
import * as React from "react";
import dayjs from "dayjs";
import _ from 'lodash'
import debounce from 'debounce-promise'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers'
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
import { ReactContext as PageDataContext } from '@components/providers/PageData'
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Head from 'next/head'
import { useRouter, usePathname } from 'next/navigation';

//* endof  Компоненты из библиотек

//* секция Наши компоненты
import EditObjectModal from './EditObjectModal'
import CreateObjectModal from './CreateObjectModal'
import UnarchiveModal from '@components/_global/UnarchiveModal'
import TemplateTable from '@components/_global/TemplateTable'
import CustomTableFooter from '@components/_global/CustomTableFooter'
import CustomTableHeader from '@components/_global/CustomTableHeader'
//* endof  Наши компоненты

export default function Main() {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения


    //* контекст
    let { state: pageData } = React.useContext(PageDataContext);
    //* endof контекст

    //* состояние
    let [selectedObject, setSelectedObject] = React.useState(null)
    let [newObject, setNewObject] = React.useState(null)
    let [defaultCompaniesOptions, setDefaultCompaniesOptions] = React.useState()

    const unarchiveModalRef = React.useRef();
    const editUserModalRef = React.useRef();
    const createUserModalRef = React.useRef();

    let [tableSettings, setTableSettings] = React.useState(_.pick(pageData, ['pageNumbersToShow', 'dataLength', 'firstPage', 'lastPage', 'selectedPage', 'pageRows', 'pageSize', 'isArchived']))
    const [isTableRefreshing, setTableRefreshing] = React.useState(false)
    //* endof состояние

    //* вычисляемые переменные, изменение состояния
    let { pageNumbersToShow, firstPage, lastPage, selectedPage, pageRows, pageSize, dataLength, isArchived } = tableSettings;
    //* endof вычисляемые переменные, изменение состояния

    //* эффекты
    React.useEffect(() => {
        loadCompaniesOptions('', setDefaultCompaniesOptions)
    }, [])
    //* endof эффекты

    //* функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики
    async function loadCompanyById(companyId, callback) {
        try {
            if (!companyId) return;
            let company = await clientHelpers.submitObject("/api/companies/getOne", { _id: companyId });
            callback(company)
        } catch (e) {
            notifications.showSystemError(e.message)
        }
    }

    async function loadCompaniesOptions(inputValue, callback) {
        try {
            let { pageRows } = await clientHelpers.submitObject("/api/companies/tableFilterAndSort", { search: inputValue, pageSize: 50, selectedPage: 1 });
            let pageRowsForSelect = pageRows.map(company => ({ value: company._id, label: commonHelpers.formatCompanyName(company) }))
            callback(pageRowsForSelect)
        } catch (e) {
            notifications.showSystemError(e.message)
        }
    }

    let debounceLoadCompaniesOptions = debounce(loadCompaniesOptions, 300);

    async function handleSetPage(value) {
        tableSettings.selectedPage = value;
        await refreshTable()
    }

    async function refreshTable() {
        try {
            setTableRefreshing(true)
            let tableSettingsToSend = _.pick(tableSettings, ['filter', 'search', 'sortConfig', 'selectedPage', 'pageSize', 'projection', 'isArchived'])
            let newTableSettings = await clientHelpers.submitObject("/api/users/tableFilterAndSort", tableSettingsToSend);
            
            setTableSettings({ ...tableSettings, ...newTableSettings })

            // при обновлении внешних данных таблицы (скорее всего raws), у таблицы теряется выставленная сортировка
        } catch (e) {
            notifications.showSystemError(e.message)
        } finally {
            setTableRefreshing(false)
        }
    }

    // custom - это запрос на сервер. Серверу для удобства юзера стоит отдавать то что запрошено +100 и -100 записей. Все внешние фильтры тоже должны будут обновлять параметр isLoading
    async function handleSetPageSize(value) {
        tableSettings.pageSize = value;
        await refreshTable()
    }

    async function handleRequestsSortCustom(field, direction) {
        tableSettings.sortConfig = { field, direction };
        await refreshTable()
    }

    async function handleSelectNewObject(key) {
        setNewObject(key)

        createUserModalRef.current.setModalOpen(true)
        // в случае с модалкой тут просто: привязать обработчик архивации, показать модалку с selectedObject. Я считаю, отдельную страницу для этого делать может быть непрактично и слишком широко (данных нет толком), если только в случае статистики по рыбе.
    }

    async function handleSelectUser(key) {
        try {

            selectedObject = key;
            setSelectedObject(key)

            if (!key) return;

            editUserModalRef.current.setModalOpen(true)
        } catch (e) {
            console.error(e);
            notifications.showSystemError(e.message)
        }
    }

    async function handleSaveUser(fields) {
        let fieldsToSave = {}

        for (let key of Object.keys(fields)) {
            if (!_.isEqual(selectedObject[key], fields[key])) {
                if (fields[key] instanceof Date) {
                    if (!dayjs(fields[key]).diff(selectedObject[key])) continue;
                }
                fieldsToSave[key] = fields[key];

            }
        }

        let updatedUser = await clientHelpers.submitObject('/api/users/update', { ...fieldsToSave, _id: selectedObject._id })
        refreshTable();
        notifications.showSuccess('Пользователь обновлен')
    }

    async function handleArchiveUser(isArchive) {
        await clientHelpers.submitObject("/api/users/archive", { _id: selectedObject._id, isArchive });

        refreshTable();
        if (isArchive) {
            notifications.showSuccess('Пользователь архивирован')
        } else {
            notifications.showSuccess('Пользователь разархивирован')
        }
    }

    async function handleCreateUser(fields) {
        await clientHelpers.submitObject('/api/users/create', fields)
        refreshTable();
        notifications.showSuccess('Пользователь создан')
    }
    //* endof обработчики

    //* данные для TemplateTable
    let tableHeaders = [
        {
            key: "_id",
            isSortable: true,
            title: "_id",
            isDefaultSort: true,
            defaultDirection: 'asc',

        },
        {
            key: "name",
            isSortable: true,
            title: "Имя"

        },
        {
            key: "username",
            isSortable: true,
            title: "Имя пользователя"

        },
        {
            key: "email",
            isSortable: true,
            title: "Email"
        },
        {
            key: "role",
            isSortable: false,
            title: "Роль"
        },
    ];

    let tableFields = React.useMemo(() => [
        {
            render: (item, idx) =>
                <td className="td name">
                    <span className="link"
                        onClick={e => handleSelectUser(item)}>
                        {item._id}{item.isArchived && ' (Архивирован)'}
                    </span>
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {item.name}
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {item.username}
                </td>
        },

        {
            render: (item, idx) =>
                <td className="td name">
                    {item.email}
                </td>
        },

        {
            render: (item, idx) =>
                <td className="td name">
                    {item.role}
                </td>
        },
    ], [])

    let tableActions = React.useMemo(() => function TableActions(row, idx) {
        return (
            <>
            </>
        );
    }, [])
    //* endof данные для TemplateTable

    return (
        <>
            <div className="w-100 pb-20 px-20">
                <div className="d-flex justify-content-between w-100 w-100">
                    <div className="w-100 pt-20 d-flex flex-column align-items-center form">
                        <CustomTableHeader {...{ tableSettings, refreshTable, isTableRefreshing, dataLength, text: 'Пользователи', handleSelectNewObject: handleSelectNewObject }} />

                        <div className="w-100 bg-white py-10 px-20 d-flex flex-column align-items-center form">

                            <TemplateTable
                                edit={false}
                                headers={tableHeaders}
                                fields={tableFields}
                                rows={pageRows || []}
                                keyField="_id"
                                emptyField="_id"
                                isAddEmptyTableItem={false}
                                showByDefault={true}
                                isActions={false}
                                actions={tableActions}
                                isBatchActions={false}
                                isRefreshTable={false}
                                isCustomSort={true}
                                requestSortCustom={handleRequestsSortCustom}
                                tableClasses="table w-100"
                                pageSizeDefault={999} // 999 - значит пагинация нестандартная, контролируется вне TemplateTable
                                isTopPages={false}
                                isBottomPages={false}
                            />

                            <CustomTableFooter
                                {...
                                {
                                    selectedPage,
                                    firstPage,
                                    lastPage,
                                    isTableRefreshing,
                                    handleSetPage,
                                    pageNumbersToShow,
                                    pageSize,
                                    handleSetPageSize
                                }}
                            />
                        </div>
                    </div>

                </div>
            </div>

            <UnarchiveModal ref={unarchiveModalRef} />
            <EditObjectModal {...{ loadCompanyById, defaultCompaniesOptions, debounceLoadCompaniesOptions, selectedObject }} onArchiveUser={handleArchiveUser} onSaveUser={handleSaveUser} ref={editUserModalRef} />
            <CreateObjectModal {...{ loadCompanyById, defaultCompaniesOptions, debounceLoadCompaniesOptions, newObject }} onCreateUser={handleCreateUser} ref={createUserModalRef} />
        </>
    )
}