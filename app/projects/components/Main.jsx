//* секция Библиотеки c функциями
import * as React from "react";
import dayjs from "dayjs";
import _ from 'lodash'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers'
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
import { ReactContext as PageDataContext } from '@components/providers/PageData'
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Head from 'next/head'
import { useRouter, usePathname } from 'next/navigation';

//* endof  Компоненты из библиотек

//* секция Наши компоненты
import EditObjectModal from './EditObjectModal'
import CreateObjectModal from './CreateObjectModal'
import UnarchiveModal from '@components/_global/UnarchiveModal'
import TemplateTable from '@components/_global/TemplateTable'
import CustomTableFooter from '@components/_global/CustomTableFooter'
import CustomTableHeader from '@components/_global/CustomTableHeader'
//* endof  Наши компоненты

export default function Main() {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения


    //* контекст
    let { state: pageData } = React.useContext(PageDataContext);
    //* endof контекст

    //* состояние
    let [selectedObject, setSelectedObject] = React.useState(null)
    let [newObject, setNewObject] = React.useState(null)

    const unarchiveModalRef = React.useRef();
    const editProjectModalRef = React.useRef();
    const createProjectModalRef = React.useRef();

    let [tableSettings, setTableSettings] = React.useState(_.pick(pageData, ['pageNumbersToShow', 'dataLength', 'firstPage', 'lastPage', 'selectedPage', 'pageRows', 'pageSize', 'isArchived']))
    const [isTableRefreshing, setTableRefreshing] = React.useState(false)
    //* endof состояние

    //* вычисляемые переменные, изменение состояния
    let { pageNumbersToShow, firstPage, lastPage, selectedPage, pageRows, pageSize, dataLength, isArchived } = tableSettings;
    //* endof вычисляемые переменные, изменение состояния

    //* эффекты

    //* endof эффекты

    //* функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики
    async function handleSetPage(value) {
        tableSettings.selectedPage = value;
        await refreshTable()
    }

    async function refreshTable() {
        try {
            setTableRefreshing(true)
            let tableSettingsToSend = _.pick(tableSettings, ['filter', 'search', 'sortConfig', 'selectedPage', 'pageSize', 'projection', 'isArchived'])
            let newTableSettings = await clientHelpers.submitObject("/api/projects/tableFilterAndSort", tableSettingsToSend);

            setTableSettings({ ...tableSettings, ...newTableSettings })

            // при обновлении внешних данных таблицы (скорее всего raws), у таблицы теряется выставленная сортировка
        } catch (e) {
            notifications.showSystemError(e.message)
        } finally {
            setTableRefreshing(false)
        }
    }

    // custom - это запрос на сервер. Серверу для удобства юзера стоит отдавать то что запрошено +100 и -100 записей. Все внешние фильтры тоже должны будут обновлять параметр isLoading
    async function handleSetPageSize(value) {
        tableSettings.pageSize = value;
        await refreshTable()
    }

    async function handleRequestsSortCustom(field, direction) {
        tableSettings.sortConfig = { field, direction };
        await refreshTable()
    }

    async function handleSelectNewObject(key) {
        setNewObject(key)

        createProjectModalRef.current.setModalOpen(true)
        // в случае с модалкой тут просто: привязать обработчик архивации, показать модалку с selectedObject. Я считаю, отдельную страницу для этого делать может быть непрактично и слишком широко (данных нет толком), если только в случае статистики по рыбе.
    }

    async function handleSelectProject(key) {
        try {

            selectedObject = key;
            setSelectedObject(key)

            if (!key) return;

            editProjectModalRef.current.setModalOpen(true)
        } catch (e) {
            console.error(e);
            notifications.showSystemError(e.message)
        }
    }

    async function handleSaveProject(fields) {
        let fieldsToSave = {}

        for (let key of Object.keys(fields)) {
            if (!_.isEqual(selectedObject[key], fields[key])) {
                if (fields[key] instanceof Date) {
                    if (!dayjs(fields[key]).diff(selectedObject[key])) continue;
                }
                fieldsToSave[key] = fields[key];

            }
        }

        let updatedProject = await clientHelpers.submitObject('/api/projects/update', { ...fieldsToSave, _id: selectedObject._id })
        refreshTable();
        notifications.showSuccess('Проект обновлен')
    }

    async function handleArchiveProject(isArchive) {
        await clientHelpers.submitObject("/api/projects/archive", { _id: selectedObject._id, isArchive });

        refreshTable();
        if (isArchive) {
            notifications.showSuccess('Проект архивирован')
        } else {
            notifications.showSuccess('Проект разархивирован')
        }
    }

    async function handleCreateProject(fields) {
        await clientHelpers.submitObject('/api/projects/create', fields)
        refreshTable();
        notifications.showSuccess('Проект создан')
    }
    //* endof обработчики

    //* данные для TemplateTable
    let tableHeaders = [
        {
            key: "name",
            isSortable: true,
            title: "Название"
        },
        {
            key: "appId",
            isSortable: true,
            title: "App ID"
        },
    ];

    let tableFields = React.useMemo(() => [
        {
            render: (item, idx) =>
                <td className="td name">
                    <span className="link"
                        onClick={e => handleSelectProject(item)}>
                        {item.name}{item.isArchived && ' (Архивирован)'}
                    </span>
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {item.appId}
                </td>
        },
    ])

    let tableActions = React.useMemo(() => function TableActions(row, idx) {
        return (
            <>
            </>
        );
    }, [])
    //* endof данные для TemplateTable

    return (
        <>
            <div className="w-100 pb-20 px-20">
                <div className="d-flex justify-content-between w-100 w-100">
                    <div className="w-100 pt-20 d-flex flex-column align-items-center form">
                        <CustomTableHeader {...{ tableSettings, refreshTable, isTableRefreshing, dataLength, text: 'Проекты', handleSelectNewObject: handleSelectNewObject }} />

                        <div className="w-100 bg-white py-10 px-20 d-flex flex-column align-items-center form">

                            <TemplateTable
                                edit={false}
                                headers={tableHeaders}
                                fields={tableFields}
                                rows={pageRows || []}
                                keyField="_id"
                                emptyField="_id"
                                isAddEmptyTableItem={false}
                                showByDefault={true}
                                isActions={false}
                                actions={tableActions}
                                isBatchActions={false}
                                isRefreshTable={false}
                                isCustomSort={true}
                                requestSortCustom={handleRequestsSortCustom}
                                tableClasses="table w-100"
                                pageSizeDefault={999} // 999 - значит пагинация нестандартная, контролируется вне TemplateTable
                                isTopPages={false}
                                isBottomPages={false}
                            />

                            <CustomTableFooter
                                {...
                                {
                                    selectedPage,
                                    firstPage,
                                    lastPage,
                                    isTableRefreshing,
                                    handleSetPage,
                                    pageNumbersToShow,
                                    pageSize,
                                    handleSetPageSize
                                }}
                            />
                        </div>
                    </div>

                </div>
            </div>

            <UnarchiveModal ref={unarchiveModalRef} />
            <EditObjectModal onArchiveProject={handleArchiveProject} selectedObject={selectedObject} onSaveProject={handleSaveProject} ref={editProjectModalRef} />
            <CreateObjectModal newObject={newObject} onCreateProject={handleCreateProject} ref={createProjectModalRef} />
        </>
    )
}