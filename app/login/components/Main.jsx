// то есть никакой оптимизации в случае даже с обычным прикреплением юзера тут не будет. Нафига тогда нужны серверные компоненты, непонятно.
//* секция Библиотеки c функциями
import * as React from "react";
import _ from 'lodash'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
import LoginBlock from './Tabs/Login'
//* endof  Наши компоненты


export default function Component() {
    return (
        <div className="w-100 pb-20 px-20">
            <div className="login-block mx-auto my-60">
                <h1 className="heading welcome align-center mb-30 size-32 color-gray3 weight-400">
                    <span>Вход в систему</span>
                </h1>
                <div className="d-flex justify-content-center">
                    <div className="w-lg-50">
                        <div className="tab-content mt-20">
                            <LoginBlock />
                        </div>
                    </div>
                </div>

            </div>

        </div>
    )
}