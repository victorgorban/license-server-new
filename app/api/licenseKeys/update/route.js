//* секция Библиотеки c функциями
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    _id: { type: String, required: true },
    projectId: String,
    companyId: String,
    userId: String,
    keyString: String,
    maxDevices: Number,
    deactivationMethod: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.deactivationMethodsForSelect.map(obj => obj.value),
    },
    activationScope: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.activationScopesForSelect.map(obj => obj.value),
    },
    deviceValidPeriod: Number,
    transmissionProtectionAesKey: {
      type: Array,
    },
    'transmissionProtectionAesKey.$': Number,
    validUntil: Date,
    // статус - для ключей без срока действия
    status: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.statusesForSelect.map(obj => obj.value),
    }
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    if (!req.user) {
      throw new Error("Сначала войдите в систему");
    }

    if (
      req.user.role != 'globalAdmin'
    ) {
      throw new Error("Недостаточно прав");
    }

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let licenseKeyData = data;

    let existinglicenseKey = await Collections.licenseKeys.findOne({ _id: licenseKeyData._id });
    if (!existinglicenseKey) {
      throw new Error("Такого лиц. ключа не существует");
    }

    let updateSet = { ...licenseKeyData };
    delete updateSet._id;
    if (_.isEqual(updateSet, {})) {
      console.log('updateSet empty')
      return apiResponses.success(null, existinglicenseKey);
    }

    let updateResult = await Collections.licenseKeys.updateOne(
      { _id: licenseKeyData._id },
      { $set: updateSet }
    );

    let updatedLicenseKey = updateResult;

    return apiResponses.success(null, updatedLicenseKey);
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}