//* секция Библиотеки c функциями
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    keyString: { type: String, required: true },
    protectionData: { type: Object, blackbox: true }
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let { keyString } = data;

    let licenseKey = await Collections.licenseKeys.findOne({
      keyString: keyString,
      isArchive: { $ne: true }
    });
    if (!licenseKey) throw new Error("Лиц. ключ не найден");

    if (licenseKey.status == 'disabled') {
      throw new Error("Лиц. ключ неактивен")
    }

    // остальное - защита, привязки - это потом уже. Для аквакультуры и этого много

    return apiResponses.success(null, { status: licenseKey.status, validUntil: licenseKey.validUntil });
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}