//* секция Библиотеки c функциями
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    _id: { type: String, required: true },
    isArchive: Boolean // архивировать или разархивировать
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    if (!req.user) {
      throw new Error("Сначала войдите в систему");
    }

    if (
      req.user.role != 'globalAdmin'
    ) {
      throw new Error("Недостаточно прав");
    }

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let licenseKeyData = data;

    let existinglicenseKey = await Collections.licenseKeys.findOne({ _id: licenseKeyData._id });
    if (!existinglicenseKey) {
      throw new Error("Компании не существует");
    }

    let updateResult = await Collections.licenseKeys.archiveOne(
      { _id: licenseKeyData._id },
      licenseKeyData.isArchive
    );

    let updatedLicenseKey = updateResult;

    return apiResponses.success(null, updatedLicenseKey);
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}