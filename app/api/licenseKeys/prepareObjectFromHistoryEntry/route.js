//* секция Библиотеки c функциями
import { cookies as nextCookies } from 'next/headers';
import NodeRSA from "node-rsa";
import path from "path";
import fs from "fs";
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    oplogId: String,
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    if (!req.user) {
      throw new Error("Сначала войдите в систему");
    }

    if (
      req.user.role != 'globalAdmin'
    ) {
      throw new Error("Недостаточно прав");
    }

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let { oplogId } = data;

    let oplogObj = await Collections.oplog.findOne(oplogId);
    if (!oplogObj) {
      throw new Error('Объект истории не найден')
    }

    let targetObject = null;
    switch (oplogObj.method) {
      case 'updateOne': case 'archiveOne':
        targetObject = oplogObj.stateBefore;
        break;
      case 'replaceOne': case 'insertOne':
        targetObject = oplogObj.actionQuery;
        break;
      case 'deleteOne': throw new Error(`Объект истории с методом ${oplogObj.method} нельзя просмотреть`)
      default: throw new Error(`Объект истории с методом ${oplogObj.method} нельзя просмотреть`)
    }

    let tempDocId = commonHelpers.randomString();
    let tempDocData = { ...targetObject, _id: tempDocId }
    await Collections.licenseKeysTemporary.collection.insertOne({
      ...tempDocData,
    });
    let updateQuery = {};

    // TODO должно работать. А теперь все это нужно долго тестить, я устал уже.
    switch (oplogObj.method) {
      case 'updateOne': case 'archiveOne':
        updateQuery = oplogObj.actionQuery;
        break;
      default: break;
    }


    if (!_.isEqual(updateQuery, {})) {
      let updateResult = await Collections.licenseKeysTemporary.collection.findOneAndUpdate({ _id: tempDocId }, updateQuery, { returnDocument: 'after' })
      return apiResponses.success(null, updateResult.value)
    } else {
      let updatedObject = await Collections.licenseKeysTemporary.collection.findOne({ _id: tempDocId })
      return apiResponses.success(null, updatedObject)
    }
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}