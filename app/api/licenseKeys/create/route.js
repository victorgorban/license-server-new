//* секция Библиотеки c функциями
import { cookies as nextCookies } from 'next/headers';
import NodeRSA from "node-rsa";
import path from "path";
import fs from "fs";
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    _id: String,
    projectId: String,
    companyId: String,
    userId: String,
    keyString: String,
    deactivationMethod: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.deactivationMethodsForSelect.map(obj => obj.value),
    },
    activationScope: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.activationScopesForSelect.map(obj => obj.value),
    },
    protectionType: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.protectionTypesForSelect.map(obj => obj.value),
    },
    maxDevices: Number,
    deviceValidPeriod: Number,
    transmissionProtectionAesKey: {
      type: Array,
    },
    'transmissionProtectionAesKey.$': Number,
    validUntil: Date,
    // статус - для ключей без срока действия
    status: {
      type: String,
      allowedValues: commonHelpers.licenseKeys.statusesForSelect.map(obj => obj.value),
      defaultValue: 'active'
    }
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    if (!req.user) {
      throw new Error("Сначала войдите в систему");
    }

    if (
      req.user.role != 'globalAdmin'
    ) {
      throw new Error("Недостаточно прав");
    }

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let licenseKeyData = data;

    let publicData = { keyString: licenseKeyData.keyString }
    if (licenseKeyData.protectionType == 'rsa') {
      let protectionSettings = {};
      licenseKeyData.protectionSettings = protectionSettings
      let key = new NodeRSA()

      key.generateKeyPair();
      let privateKey = key.exportKey('pkcs8-private-pem');
      let publicKey = key.exportKey('pkcs8-public-pem');
      protectionSettings.rsaPublicKey = publicKey;
      protectionSettings.rsaPrivateKey = privateKey;
      licenseKeyData.protectionSettings = protectionSettings;
    } else if (licenseKeyData.protectionType == 'aes') {
      throw new Error('Функция еще не реализована')
    }

    let tempFolderId = commonHelpers.randomString();
    let publicDataFolder = `${process.env.PUBLIC_TEMP_DIR}/${tempFolderId}`;
    let publicDataPath = path.join(publicDataFolder, "licenseInfo.json");
    let publicDataUrl = `${process.env.PUBLIC_TEMP_LINK}/${tempFolderId}/licenseInfo.json`;
    await fs.promises.mkdir(publicDataFolder, { recursive: true });
    await fs.promises.writeFile(publicDataPath, JSON.stringify(publicData, null, 2), 'utf-8')

    let insertedlicenseKey = await Collections.licenseKeys.insertOne({
      ...licenseKeyData,
    });

    // TODO кстати, надо бы встроить активацию в мои распространяемые проекты (там где нет исходников), чтобы можно было их пощупать недельку, а потом всё.
    return apiResponses.success(null, { licenseKey: insertedlicenseKey, publicDataUrl });
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}