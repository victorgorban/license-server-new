//* секция Библиотеки c функциями
import { cookies as nextCookies } from 'next/headers';
import NodeRSA from "node-rsa";
import path from "path";
import fs from "fs";
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    _id: String,
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    if (!req.user) {
      throw new Error("Сначала войдите в систему");
    }

    if (
      req.user.role != 'globalAdmin'
    ) {
      throw new Error("Недостаточно прав");
    }

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let { _id: keyId } = data;
    let licenseKey = await Collections.licenseKeys.findOne(keyId)
    if (!licenseKey) {
      throw new Error("Лиц. ключ не найден")
    }

    let publicData = {
      status: licenseKey.status,
      keyString: licenseKey.keyString,
      validUntil: commonHelpers.formatDate(licenseKey.validUntil)
    }

    let tempFolderId = commonHelpers.randomString();
    let publicDataFolder = `${process.env.PUBLIC_TEMP_DIR}/${tempFolderId}`;
    let publicDataPath = path.join(publicDataFolder, "licenseInfo.json");
    let publicDataUrl = `${process.env.PUBLIC_TEMP_LINK}/${tempFolderId}/licenseInfo.json`;
    await fs.promises.mkdir(publicDataFolder, { recursive: true });
    await fs.promises.writeFile(publicDataPath, JSON.stringify(publicData, null, 2), 'utf-8')

    return apiResponses.success(null, { fileUrl: publicDataUrl });
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}