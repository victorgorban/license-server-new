//* секция Библиотеки c функциями
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
import _ from 'lodash'
import dayjs from 'dayjs'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
import * as licenseHelpers from '@serverHelpers/licenseKeys'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    keyString: { type: String, required: true },
    deviceData: { type: oneOf(String, Object), blackbox: true, required: true }
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * Система рассчитана на универсальную активацию любого устройства, без привязки к типу, размеру или ОС. 
 * Поэтому здесь используется не ключ винды, а серийник компьютера и серийный номер процессора.
 * При желании, конечно, ничего не мешает изменить алгоритм на использование других полей.
 * AES в основном полезен только для онлайн-проверок. В оффлайне для проверки придется юзать расшифрование - где безопасность? Поэтому смысл есть только в онлайн-проверке.
 */
export async function POST(req, { params }) {
  try {
    // TODO ключ пользователя при активации пусть посылает уведомления просто, на email и на телефон.
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let { keyString, deviceData } = data;

    // пипец, и весь этот километровый синтаксис вместо того, чтобы написать join x by y?
    let licenseKeyAggregation = await Collections.licenseKeys.collection.aggregate([
      {
        $match: {
          keyString: keyString,
          isArchive: { $ne: true }
        }
      },
      {
        $lookup: {
          as: 'companyData',
          from: 'companies',
          localField: 'companyId',
          foreignField: '_id'
        }
      },
      {
        $lookup: {
          as: 'userData',
          from: 'users',
          localField: 'userId',
          foreignField: '_id'
        }
      }
    ])
    let licenseKey = licenseKeyAggregation[0];
    if (!licenseKey) throw new Error("Лиц. ключ не найден");

    console.log('keyString', keyString, licenseKey.keyString)

    licenseKey.companyData = licenseKey.companyData?.[0];
    licenseKey.userData = licenseKey.userData?.[0];

    if (licenseKey.protectionType != 'rsa') {
      throw new Error("Здесь обрабатывается только ключи с защитой RSA.")
    }

    let errorMessageNotCrush = ''

    if (licenseKey.status == 'disabled') {
      errorMessageNotCrush = "Лиц. ключ неактивен"
    }

    if (licenseKey.deactivationMethod == 'validUntil' && licenseKey.validUntil && licenseKey.validUntil < new Date()) {
      errorMessageNotCrush = `Время действия лиц. ключа истекло ${commonHelpers.formatDate(licenseKey.validUntil)}`
    }

    // теперь дешифрация и парсинг данных.
    if (licenseKey.transmissionProtectionAesKey) {
      deviceData = licenseHelpers.decryptAesPlain(Buffer.from(deviceData, 'base64'), Buffer.from(licenseKey.transmissionProtectionAesKey), licenseKey.keyString);
      deviceData = JSON.parse(deviceData);
    }

    if (licenseKey.projectAppId != deviceData.appId) {
      console.log('Ключ от другого приложения', licenseKey.projectAppId, deviceData.appId)
      throw new Error("Этот ключ от другого приложения")
    }

    let deviceDataKeysNeeded = ['system', 'cpu']
    let deviceDataNeeded = _.pick(deviceData, deviceDataKeysNeeded)
    for (let [key, value] of Object.entries(deviceDataNeeded)) {
      if (!value) throw new Error(`Нет необходимой информации об устройстве, ключ ${key}`)
    }

    licenseKey.devices = licenseKey.devices || [];
    let existingDevice = licenseKey.devices.find(device => _.isEqual(_.pick(device.deviceInfo, deviceDataKeysNeeded), deviceDataNeeded))
    if (!existingDevice) {
      if (licenseKey.devices && licenseKey.maxDevices && licenseKey.maxDevices <= licenseKey.devices.length) {
        throw new Error(`Достигнут максимум устройств, максимум: ${licenseKey.maxDevices}`)
      }
    }

    let validUntil = licenseKey.validUntil;
    let signature;
    let newDevice = null;
    if (existingDevice) {
      signature = existingDevice.signature;
      console.log('in existingDevice, deactivationMethod', licenseKey.deactivationMethod, licenseKey.deactivationMethod == 'perDevice')
      if (licenseKey.deactivationMethod == 'perDevice') {
        validUntil = existingDevice.validUntil
        console.log('in existingDevice, perDevice', existingDevice.validUntil)
        if (!existingDevice.validUntil) {
          errorMessageNotCrush = 'Не указано время действия ключа для выбранного способа активации'
        } else if (existingDevice.validUntil && existingDevice.validUntil < new Date()) {
          errorMessageNotCrush = `Время действия лиц. ключа истекло ${commonHelpers.formatDate(existingDevice.validUntil)}`
        }
      }

    } else {
      if (licenseKey.deactivationMethod == 'perDevice') {
        validUntil = dayjs().add(licenseKey.deviceValidPeriod, 'days').toDate()
      }
      let signatureParams = { privateKey: licenseKey.protectionSettings.rsaPrivateKey, validUntil, appId: licenseKey.projectAppId, deviceData: deviceDataNeeded }
      signature = serverHelpers.licenseKeys.generateRsaSignatureAppId(signatureParams)
      newDevice = {
        deviceInfo: deviceDataNeeded,
        activationDate: new Date(),
        signature
      }
      if (licenseKey.deactivationMethod == 'perDevice') {
        // device.validUntil нужно проверять только если licenseKey.deactivationMethod == 'perDevice'. Само поле device.validUntil может быть устаревшим (например, если deactivationMethod изменили после выставления device.validUntil)
        console.log("licenseKey.deactivationMethod == 'perDevice', newDevice.validUntil", newDevice.validUntil)
        newDevice.validUntil = validUntil
      }

      // TODO необходимо проверить активацию. А также проверить вообще работу изменения deactivationMethod. C perDevice на остальные хотя бы. Для существующего девайса и для нового.

      console.log('device registration, newDevice.validUntil', newDevice.validUntil, licenseKey.deactivationMethod)

      await Collections.licenseKeys.updateOne(licenseKey._id, { $push: { devices: newDevice } })
    }

    let response = { ..._.pick(licenseKey, ["keyString", "validUntil", "companyData", "userData", "activationScope", "maxDevices"]), signature, status: licenseKey.status };
    response.validUntil = commonHelpers.formatDate(response.validUntil)
    if (licenseKey.protectionType == 'rsa') {
      response.publicKey = licenseKey.protectionSettings.rsaPublicKey;
    }
    let encryptedResponse = response;
    if (licenseKey.transmissionProtectionAesKey) {
      encryptedResponse = licenseHelpers.encryptAesPlain(JSON.stringify(encryptedResponse), Buffer.from(licenseKey.transmissionProtectionAesKey), licenseKey.keyString);
      encryptedResponse = encryptedResponse.toString('base64');
    }
    if (errorMessageNotCrush) {
      console.log('in errorMessageNotCrush', errorMessageNotCrush)
      return apiResponses.error(errorMessageNotCrush, encryptedResponse);
    }
    return apiResponses.success(null, encryptedResponse);
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}