//* секция Библиотеки c функциями
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    filter: { type: Object, blackbox: true },
    search: String,
    sortConfig: Object,
    "sortConfig.field": String,
    "sortConfig.direction": { type: String, allowedValues: ["asc", "desc"] }, // asc, desc
    selectedPage: Integer,
    pageSize: Integer,
    // если true, то вернуть архивированные записи. Если false, то актуальные записи
    isArchived: Boolean,
    // объект-фильтр полей результата. Оставьте пустым, чтобы получить все поля. Дайте объект типа {createdAt: 1} чтобы получить только указанные поля. Дайте объект типа {createdAt: 0} чтобы получить все поля, кроме указанных.
    projection: { type: Object, blackbox: true },
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params } = {}) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    if (!req.user) {
      throw new Error("Сначала войдите в систему");
    }

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    data.filter = {
      collectionName: Collections.licenseKeys.collectionName,
      'findQuery._id': params.id
    }
    data.projection = _.clone(serverHelpers.oplog.tableProjection)

    let tableResult = await serverHelpers.tableFilterAndSort({
      collectionClass: Collections.oplog,
      pageSize: 10,
      data
    })

    return apiResponses.success(null, tableResult);
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}