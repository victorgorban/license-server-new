//* секция Библиотеки c функциями
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    sourceId: { type: String, required: true },
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    if (!req.user) {
      throw new Error("Сначала войдите в систему");
    }

    if (
      req.user.role != 'globalAdmin'
    ) {
      throw new Error("Недостаточно прав");
    }

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let { sourceId } = data;
    let objectId = params.id;

    let sourceObj = await Collections.licenseKeysTemporary.findOne(sourceId);
    console.log('sourceObj', sourceObj, sourceId)
    if(!sourceObj){
      throw new Error('Исходный объект не найден')
    }
    await Collections.licenseKeys.replaceOne({_id: objectId}, {...sourceObj, _id: objectId})

    return apiResponses.success(null, null);
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}
