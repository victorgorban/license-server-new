//* секция Библиотеки c функциями
import { cookies as nextCookies } from 'next/headers';
import uniqid from "uniqid";
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import { hashPassword } from "@serverHelpers/passwordUtil";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    _id: { type: String },
    username: { type: String },
    name: { type: String },
    email: String,
    phone: String,
    companyId: String,
    role: {
      type: String,
      allowedValues: commonHelpers.users.rolesForSelect.map(obj => obj.value),
    },
    password: { type: String },
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    if (!req.user) {
      throw new Error("Сначала войдите в систему");
    }

    if (
      req.user.role != 'globalAdmin'
    ) {
      throw new Error("Недостаточно прав");
    }

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let user = await Collections.users.findOne({
      _id: data._id,
    });
    if (!user) throw new Error("Пользователь не найден.");

    if (
      user.role == "globalAdmin" &&
      !req.user.role == "globalAdmin"
    ) {
      throw new Error(
        "Данные глобального админа может редактировать только глобальный админ"
      );
    }

    let updateSet = { ...data, _id: undefined };

    if (updateSet.username) {
      let usernameUser = await Collections.users.findOne({
        username: data.username,
      });

      if (usernameUser && usernameUser._id != req.user._id) {
        throw new Error("Пользователь с этим username уже существует");
      }
    }

    if (updateSet.password) {
      updateSet.password = await hashPassword(updateSet.password);
    }

    let updateResult = await Collections.users.updateOne(
      { _id: data._id },
      { $set: updateSet }
    );

    let updatedUser = updateResult;

    return apiResponses.success(null, updatedUser);
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}