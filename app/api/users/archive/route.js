//* секция Библиотеки c функциями
import { cookies as nextCookies } from 'next/headers';
import uniqid from "uniqid";
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    _id: { type: String, required: true },
    isArchive: Boolean // архивировать или разархивировать
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    if (!req.user) {
      throw new Error("Сначала войдите в систему");
    }

    if (
      req.user.role != 'globalAdmin'
    ) {
      throw new Error("Недостаточно прав");
    }

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let userData = data;

    let existingUser = await Collections.users.findOne({ _id: userData._id });
    if (!existingUser) {
      throw new Error("Пользователь не существует");
    }

    let updateResult = await Collections.users.archiveOne(
      { _id: userData._id },
      userData.isArchive
    );

    let updatedUser = updateResult;

    return apiResponses.success(null, updatedUser);
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}