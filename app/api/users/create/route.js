//* секция Библиотеки c функциями
import { cookies as nextCookies } from 'next/headers';
import uniqid from "uniqid";
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import { hashPassword } from "@serverHelpers/passwordUtil";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    username: { type: String, required: true },
    name: { type: String, required: true },
    email: String,
    phone: String,
    companyId: String,
    role: {
      type: String,
      allowedValues: commonHelpers.users.rolesForSelect.map(obj => obj.value),
    },
    password: { type: String, required: true },
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);

    if (!req.user) {
      throw new Error("Сначала войдите в систему");
    }

    if (
      req.user?.role != "globalAdmin"
    ) {
      throw new Error("Недостаточно прав");
    }

    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let { username, email, password, companyId } = data;

    let usersMatch = [{ username }];
    if (email) {
      usersMatch.push({ email });
    }

    let existingUser = await Collections.users.findOne({
      $or: usersMatch,
    });

    if (existingUser)
      return apiResponses.error(
        "Указанное имя пользователя или email уже есть в системе"
      );

    let passwordHash = await hashPassword(password);
    let userId = Collections.users.generateDocumentId();

    let userObject = {
      _id: userId,
      ...data,
      password: passwordHash,
      companyId: companyId || req.user.companyId, // админ компании может регистрировать юзеров только своей компании.
      loginTokens: [],
    };
    let insertedUser = await Collections.users.insertOne(userObject);

    return apiResponses.success(null, insertedUser);
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}