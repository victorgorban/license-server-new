//* секция Библиотеки c функциями
import { cookies as nextCookies } from 'next/headers';
import uniqid from "uniqid";
import SimpleSchema, { Integer, Any, oneOf } from "simpl-schema";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import middlewares from './middlewares'
import * as serverHelpers from "@serverHelpers";
import * as commonHelpers from "@commonHelpers";
import { verifyPassword } from "@serverHelpers/passwordUtil";
import * as apiResponses from "@serverHelpers/responses";
import * as Collections from "@src/ORM/Collections";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

export let resultSchema = {};

export let dataSchema = new SimpleSchema(
  {
    login: { type: String, required: true },
    password: { type: String, required: true },
  },
  serverHelpers.simpleSchemaOptions
);

/**
 * 
 */
export async function POST(req, { params }) {
  try {
    let cookies = nextCookies();
    let data = await serverHelpers.parseJsonBody(req);
    await middlewares(req, data);
    
    dataSchema.clean(data, {});
    dataSchema.validate(data, {});

    let token = commonHelpers.randomString();
    let findQuery = {
      $or: [{ username: data.login }, { email: data.login }],
    };
    let user = await Collections.users.findOne(findQuery);
    if (!user) return apiResponses.error("Пользователь не найден. Войдите в систему.");

    if (!(await verifyPassword(data.password, user.password?.buffer)))
      return apiResponses.error("Неверный пароль");

    let updateInfo = await Collections.users.updateOne(findQuery, {
      $push: {
        loginTokens: token,
      },
    });

    let updatedUser = updateInfo;
    delete updatedUser.password;

    cookies.set("user_id", user._id)
    cookies.set("token", token)

    return apiResponses.success(null, {
      userId: user._id,
      token,
      user: updatedUser,
    });
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e);
  }
}