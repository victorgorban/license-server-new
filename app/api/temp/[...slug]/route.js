//* секция Библиотеки c функциями
import { NextRequest, NextResponse } from 'next/server'
import mime from 'mime-types'
import fs from 'fs'
import path from 'path'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as apiResponses from "@serverHelpers/responses";
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты

/**
 * отправка динамического файла. Папка _temp не работает (т.к. out of routing), пришлось переименовать в temp
 */
export async function GET(req, { params }) {
  try {
    console.log('in temp/..slug', req.url, params)
    let pathToFile = path.join(process.env.PUBLIC_TEMP_DIR, ...params.slug)
    console.log('pathToFile', pathToFile)
    let fileBuffer = await fs.promises.readFile(pathToFile)
    let contentType = mime.contentType(path.extname(pathToFile))
    console.log('contentType of', pathToFile, contentType)

    const response = new NextResponse(fileBuffer)
    response.headers.set('content-type', contentType);
    return response;
  } catch (e) {
    console.error(e);
    return apiResponses.error(e.message || e, null, 500);
  }
}
