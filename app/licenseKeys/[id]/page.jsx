// 'use client'
// то есть никакой оптимизации в случае даже с обычным прикреплением юзера тут не будет. Нафига тогда нужны серверные компоненты, непонятно.
//* секция Библиотеки c функциями
import * as React from "react";
import _ from 'lodash'
import EJSON from 'ejson'
import { redirect } from 'next/navigation'
import { EffectorNext } from "@effector/next";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as serverHelpers from '@serverHelpers'
import * as commonHelpers from '@commonHelpers'
import * as Collections from "@src/ORM/Collections";
import middlewares from './middlewares'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
import PageContent from './PageContent'
//* endof  Наши компоненты

export const metadata = {
  title: 'Редактирование'
}


export default async function Page(pageParams) {
  //* библиотеки и неизменяемые значения
  let params = await getPageData(pageParams);
  //* endof библиотеки и неизменяемые значения


  //* контекст

  //* endof контекст

  //* состояние

  //* endof состояние

  //* вычисляемые переменные, изменение состояния

  //* endof вычисляемые переменные, изменение состояния

  //* эффекты

  //* endof эффекты

  //* функции-хелперы, НЕ ОБРАБОТЧИКИ

  //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


  //* обработчики

  //* endof обработчики

  return (
    <PageContent {...params} />
  );
}


export async function getPageData(pageParams) {
  let data = {};
  await middlewares(pageParams, data);

  let propUser = pageParams.user || null;

  if (!propUser) {
    return redirect("/not-allowed")
  }

  let selectedObject, defaultCompaniesOptions, defaultProjectsOptions, defaultUsersOptions;

  await Promise.all([
    Collections.licenseKeys.findOne({ _id: pageParams.params.id }).then(obj => selectedObject = obj),
    serverHelpers.tableFilterAndSort({ collectionClass: Collections.projects }).then(obj => {
      let pageRowsForSelect = obj.pageRows.map(obj => ({ value: obj._id, appId: obj.appId, label: obj.name }));
      defaultProjectsOptions = pageRowsForSelect
    }),
    serverHelpers.tableFilterAndSort({ collectionClass: Collections.users }).then(obj => {
      let pageRowsForSelect = obj.pageRows.map(obj => ({ value: obj._id, label: obj.name }));
      defaultUsersOptions = pageRowsForSelect
    }),
    serverHelpers.tableFilterAndSort({ collectionClass: Collections.companies }).then(obj => {
      let pageRowsForSelect = obj.pageRows.map(obj => ({ value: obj._id, label: commonHelpers.formatCompanyName(obj) }));
      defaultCompaniesOptions = pageRowsForSelect
    }),
  ])

  selectedObject.historyTableSettings = await serverHelpers.tableFilterAndSort({
    collectionClass: Collections.oplog,
    data: {
      projection: _.clone(serverHelpers.oplog.tableProjection),
      pageSize: 10,
      filter: {
        collectionName: Collections.licenseKeys.collectionName,
        'findQuery._id': selectedObject._id
      }
    }
  })
  // TODO проработать крайний вариант, когда выбираем новейшую запись и старейшую (где создание). Создание нельзя отменить, т.к. берется stateBefore.

  let pageData = { selectedObject, defaultCompaniesOptions, defaultProjectsOptions, defaultUsersOptions };

  return {
    user: EJSON.stringify(propUser),
    pageData: EJSON.stringify(pageData),
  }
}