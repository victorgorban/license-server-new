//* секция Библиотеки c функциями
import * as React from "react";
import ReactDOM from "react-dom";
import _ from 'lodash'
import debounce from 'debounce-promise'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers'
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Link from 'next/link'

//* endof  Компоненты из библиотек

//* секция Наши компоненты
import TemplateTable from '@components/_global/TemplateTable'
import CustomTableFooter from '@components/_global/CustomTableFooter'
import CustomTableHeader from '@components/_global/CustomTableHeader'
import { TextAreaInput } from "@root/components/forms";
import RoundIcon from '@components/wrappers/RoundIcon'
import Tooltip from '@components/_global/Tooltip'
//* endof  Наши компоненты

// TODO убедиться что блок не рендерится когда selectedObject остается неизменным
export default function Component({ selectedObject, setSelectedHistoryEntry }) {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения

    //* контекст
    //* endof контекст

    //* состояние
    let [tableSettings, setTableSettings] = React.useState(selectedObject.historyTableSettings || {})
    const [isTableRefreshing, setTableRefreshing] = React.useState(false)
    const [isHistoryLoading, setHistoryLoading] = React.useState(false)
    //* endof состояние

    //* вычисляемые переменные, изменение состояния
    let { pageNumbersToShow, firstPage, lastPage, selectedPage, pageRows, pageSize, dataLength, isArchived } = tableSettings;
    //* endof вычисляемые переменные, изменение состояния
    //* endof эффекты

    //* функции-хелперы, НЕ ОБРАБОТЧИКИ
    function getHistoryDescription(item) {
        switch (item.method) {
            case 'insertOne': return 'Создание объекта'
            case 'replaceOne': return 'Замена объекта'
            case 'deleteOne': return 'Окончательное удаление объекта'
            case 'archiveOne': {
                return 'Архивация или разархивация объекта'
            }
            case 'updateOne': {
                if (item.actionQuery?.$push?.devices) {
                    return 'Активация устройства'
                } else if (item.actionQuery?.$pull?.devices) {
                    return 'Деактивация устройства'
                } else return 'Обновление объекта'
            }

            default: return item.method
        }
    }
    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* обработчики
    async function handleSelectHistoryEntry(item) {
        try {
            if (item.method == 'deleteOne') {
                return notifications.showUserError('Нельзя просмотреть состояние после удаления объекта')
            }

            document.documentElement.scrollTo({
                top: 0,
                left: 0,
                behavior: "smooth"
            });

            let objectFromHistoryItem = await clientHelpers.submitObject(`/api/licenseKeys/prepareObjectFromHistoryEntry`, { oplogId: item._id });

            await setSelectedHistoryEntry(objectFromHistoryItem);

            notifications.showInfo('Изменения применены. Вы видите состояние сразу после выбранного действия.')
        } catch (e) {
            notifications.showSystemError(e.message)
        }
    }

    async function handleSetPage(value) {
        tableSettings.selectedPage = value;
        await refreshTable()
    }

    async function refreshTable() {
        try {
            setTableRefreshing(true)
            let tableSettingsToSend = _.pick(tableSettings, ['filter', 'search', 'sortConfig', 'selectedPage', 'pageSize', 'projection', 'isArchived'])
            let newTableSettings = await clientHelpers.submitObject(`/api/licenseKeys/${selectedObject._id}/historyTableFilterAndSort`, tableSettingsToSend);
            setTableSettings({ ...tableSettings, ...newTableSettings })
        } catch (e) {
            console.log('Ошибка в RefreshTable', e.message)
            notifications.showSystemError(e.message)
        } finally {
            setTableRefreshing(false)
        }
    }

    async function handleSetPageSize(value) {
        tableSettings.pageSize = value;
        await refreshTable()
    }

    async function handleRequestsSortCustom(field, direction) {
        tableSettings.sortConfig = { field, direction };
        await refreshTable()
    }
    //* endof обработчики

    //* данные для TemplateTable
    let tableHeaders = [
        {
            key: "createdAt",
            isSortable: true,
            title: "Дата"
        },
        {
            key: "method",
            isSortable: true,
            title: "Метод"
        },
        {
            key: "actionQuery",
            isSortable: false,
            title: "Запрос"
        },
    ];

    let tableFields = React.useMemo(() => [
        {
            render: (item, idx) =>
                <td className="td name">
                    <span className="link"
                        onClick={e => handleSelectHistoryEntry(item)}>
                        {commonHelpers.formatDateFull(item.createdAt)}
                    </span>
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {getHistoryDescription(item)}
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    <TextAreaInput
                        maxLength={99999}
                        readOnly
                        value={`db.licenseKeys.${item.method}(${JSON.stringify(item.findQuery, null, 2)},${JSON.stringify(item.actionQuery, null, 2)});`}
                        style={{ minWidth: 300, minHeight: 100 }}
                    >

                    </TextAreaInput>
                </td>
        },
    ])

    let tableActions = React.useMemo(() => function TableActions(row, idx) {
        return (
            <>
            </>
        );
    }, [])
    //* endof данные для TemplateTable

    return (
        <>
            <div className="w-100 pb-20">
                <div className="d-flex justify-content-between w-100 w-100">
                    <div className="w-100 pt-20 d-flex flex-column align-items-center form">
                        <CustomTableHeader {...{
                            tableSettings,
                            refreshTable,
                            isTableRefreshing: isTableRefreshing || isHistoryLoading,
                            dataLength,
                            TooltipBlock: () => <Tooltip
                                placement="top"
                                content={"Выберите запись истории. Отобразится состояние сразу ПОСЛЕ этого действия. Список активаций при этом будет соответствовать выбранному состоянию. При сохранении сохраняется отображенное состояние."}
                            >
                                <RoundIcon color="brand1" iconName="info" />
                            </Tooltip>,
                            text: 'История изменений',
                            isActions: false
                        }} />

                        <div className="w-100 bg-white py-10 px-20 d-flex flex-column align-items-center form">
                            <TemplateTable
                                edit={false}
                                headers={tableHeaders}
                                fields={tableFields}
                                rows={pageRows || []}
                                keyField="_id"
                                emptyField="_id"
                                isAddEmptyTableItem={false}
                                showByDefault={true}
                                isActions={false}
                                actions={tableActions}
                                isBatchActions={false}
                                isRefreshTable={false}
                                isCustomSort={true}
                                requestSortCustom={handleRequestsSortCustom}
                                tableClasses="table w-100"
                                pageSizeDefault={999} // 999 - значит пагинация нестандартная, контролируется вне TemplateTable
                                isTopPages={false}
                                isBottomPages={false}
                            />

                            <CustomTableFooter
                                {...
                                {
                                    selectedPage,
                                    firstPage,
                                    lastPage,
                                    isTableRefreshing,
                                    handleSetPage,
                                    pageNumbersToShow,
                                    pageSize,
                                    handleSetPageSize
                                }}
                            />
                        </div>
                    </div>

                </div>
            </div >
        </>
    )
}