//* секция Библиотеки c функциями
import * as React from "react";
import dayjs from "dayjs";
import _ from 'lodash'
import delay from 'delay'
import debounce from 'debounce-promise'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers'
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Link from 'next/link'
import { useRouter, usePathname } from 'next/navigation';

//* endof  Компоненты из библиотек

//* секция Наши компоненты
import Button from '@components/animated/Button'
import Form from '../../components/Form'
import ArchiveModal from '@components/_global/ArchiveModal'
import UnarchiveModal from '@components/_global/UnarchiveModal'
import ActivationsBlock from './ActivationsBlock'
import HistoryBlock from './HistoryBlock'
import Tooltip from '@components/_global/Tooltip'
//* endof  Наши компоненты

export default function Main({ pageData }) {
    //* библиотеки и неизменяемые значения

    //* endof библиотеки и неизменяемые значения


    //* контекст
    //* endof контекст

    //* состояние
    const router = useRouter();
    const [isSubmitting, setSubmitting] = React.useState(false)
    const [isHistorySubmitting, setHistorySubmitting] = React.useState(false)
    let [selectedHistoryEntry, setSelectedHistoryEntry] = React.useState()
    let [selectedObject, setSelectedObject] = React.useState(pageData.selectedObject)
    // просто чтобы заставить formRef.current отобразиться
    const [isComponentMounted, setComponentMounted] = React.useState(false)
    const [selectedSectionName, setSelectedSectionName] = React.useState('activations')

    const archiveModalRef = React.useRef()
    const unarchiveModalRef = React.useRef()
    const formRef = React.useRef();

    //* endof состояние

    //* вычисляемые переменные, изменение состояния
    const defaultCompaniesOptions = pageData.defaultCompaniesOptions
    const defaultUsersOptions = pageData.defaultUsersOptions
    const defaultProjectsOptions = pageData.defaultProjectsOptions


    const logSections = selectedObject ? [
        {
            name: 'activations',
            title: `Активации`,
            block: <ActivationsBlock selectedObject={selectedHistoryEntry ? selectedHistoryEntry : selectedObject} />
        },
        {
            name: 'history',
            title: `История изменений`,
            block: <HistoryBlock {...{ selectedObject, setSelectedHistoryEntry }} />
        },
    ] : null;
    //* endof вычисляемые переменные, изменение состояния

    //* эффекты
    React.useEffect(() => {
        setComponentMounted(true); // просто чтобы обновился интерфейс и правильно поставился formRef.current
    }, [])
    //* endof эффекты

    //* функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики
    function selectSection(sectionName) {
        setSelectedSectionName(sectionName);
    }

    async function saveHistoryEntry() {
        try {
            setHistorySubmitting(true)
            await clientHelpers.submitObject(`/api/licenseKeys/${selectedObject._id}/replaceWithSourceObject`, { sourceId: selectedHistoryEntry._id });
            setSelectedObject({ ...selectedHistoryEntry, _id: selectedObject._id });
            setSelectedHistoryEntry(null);
            notifications.showSuccess('Откат изменений до выбранной записи успешен.')
        } catch (e) {
            notifications.showSystemError(e.message)
        } finally {
            setHistorySubmitting(false)
        }
    }

    async function downloadPublicKey() {
        try {
            let { fileUrl } = await clientHelpers.submitObject("/api/licenseKeys/downloadPublicKey", { _id: selectedObject._id });
            clientHelpers.download(fileUrl, 'licenseInfo.json');
            notifications.showSuccess('Файл ключа скачан')
        } catch (e) {
            notifications.showSystemError(e.message)
        }
    }

    async function handleSave() {
        try {
            setSubmitting(true)

            let fieldsToSave = {}
            let fields = _.cloneDeep(formRef.current.getFormState()); // в модалках копирование было в обработчике внутри модалки, а тут нужно это делать внутри.
            if (fields.validUntil) {
                fields.validUntil = new Date(fields.validUntil)
            }

            if (fields.deviceValidPeriod) {
                fields.deviceValidPeriod = +fields.deviceValidPeriod || 0
            }

            if (fields.transmissionProtectionAesKey) {
                fields.transmissionProtectionAesKey = JSON.parse(fields.transmissionProtectionAesKey)
            }

            for (let key of Object.keys(fields)) {
                if (!_.isEqual(selectedObject[key], fields[key])) {
                    if (fields[key] instanceof Date) {
                        if (!dayjs(fields[key]).diff(selectedObject[key])) continue;
                    }
                    fieldsToSave[key] = fields[key];
                }
            }

            console.log('before submitObject', fieldsToSave);

            let updatedLicenseKey = await clientHelpers.submitObject('/api/licenseKeys/update', { ...fieldsToSave, _id: selectedObject._id })
            setSelectedObject(updatedLicenseKey)

            notifications.showSuccess('Лиц. ключ обновлен')
        } catch (e) {
            notifications.showSystemError(e.message)
        } finally {
            setSubmitting(false)
        }
    }

    async function handleArchive(isArchive) {
        try {
            let modalRef = isArchive ? archiveModalRef : unarchiveModalRef;
            setSubmitting(true);

            let archiveActions = {
                resolve: null,
                reject: null
            }

            let archiveRequestPromise = new Promise((resolve, reject) => {
                archiveActions.resolve = resolve;
                archiveActions.reject = reject;
            })

            modalRef.current.setPromiseActions(archiveActions);
            modalRef.current.setModalOpen(true);

            let archiveRequestResult = await archiveRequestPromise

            if (archiveRequestResult) {
                await onArchiveObject?.(isArchive)
            }

        } catch (e) {
            notifications.showSystemError(e.message)
        } finally {
            setSubmitting(false);
        }
    }

    async function onArchiveObject(isArchive) {
        let updatedLicenseKey = await clientHelpers.submitObject("/api/licenseKeys/archive", { _id: selectedObject._id, isArchive });

        setSelectedObject(updatedLicenseKey)

        if (isArchive) {
            notifications.showSuccess('Лиц. ключ архивирован')
        } else {
            notifications.showSuccess('Лиц. ключ разархивирован')
        }
    }

    //* endof обработчики

    return (
        <>
            <div className="w-100 pb-20 px-20" style={{

            }}>
                <div className="d-flex justify-content-between w-100 w-100">
                    <div className="w-100 pt-20 d-flex flex-column align-items-center form">
                        <div className="page-header w-100 mb-20 d-flex justify-content-between color-gray3 ">
                            <h2 className="weight-400 size-21">Лиц. ключ {selectedObject?.keyString}{selectedHistoryEntry && ' (РЕЖИМ ИСТОРИИ)'}{selectedHistoryEntry ? selectedHistoryEntry?.isArchived && ' (АРХИВИРОВАН)' : selectedObject?.isArchived && ' (АРХИВИРОВАН)'}</h2>

                            <div className="actions w-100 w-md-50 w-xl-33 d-flex justify-content-end">
                                <Link
                                    href="/licenseKeys"
                                    className="btn style-default w-fit-content ml-10 size-14"
                                >
                                    Назад к списку
                                </Link>
                            </div>
                        </div>

                        <div className="w-100 bg-white py-10 px-20 d-flex flex-column align-items-center form">
                            {!selectedHistoryEntry ?
                                <Form
                                    ref={formRef}
                                    mode="edit"
                                    selectedObject={selectedObject}
                                    isDisabled={selectedObject?.isArchived}
                                    disabledFields={['keyString', 'protectionType', 'deactivationMethod']}
                                    {...{
                                        defaultCompaniesOptions,
                                        defaultProjectsOptions,
                                        defaultUsersOptions
                                    }}
                                />
                                :
                                <Form
                                    ref={formRef}
                                    mode="edit"
                                    selectedObject={selectedHistoryEntry}
                                    isDisabled={true}
                                    {...{
                                        defaultCompaniesOptions,
                                        defaultProjectsOptions,
                                        defaultUsersOptions
                                    }}
                                />
                            }

                            {!selectedHistoryEntry ?
                                <div className="footer w-100 d-flex justify-content-between align-items-center mt-0">
                                    <div>
                                        {!selectedObject?.isArchived ?
                                            <Button
                                                variant="danger"
                                                className={`mx-05 py-20 px-20 size-16 weight-500`}
                                                isLoading={isSubmitting}
                                                onClick={e => handleArchive(true)}
                                                disabled={isSubmitting}
                                            >
                                                Архивировать
                                            </Button>
                                            :
                                            null
                                        }
                                    </div>

                                    <div className="d-flex">
                                        <Tooltip
                                            placement="top"
                                            content="Скачать публичный ключ. Сначала нужно сохранить изменения."
                                        >
                                            <Button
                                                variant="green"
                                                className={`mx-05 w-fit-content py-20 px-20 size-16 weight-500`}
                                                style={{ minWidth: 50 }}
                                                onClick={downloadPublicKey}
                                            >
                                                <i className="fa fa-download"></i>
                                            </Button>
                                        </Tooltip>

                                        {!selectedObject?.isArchived ?
                                            <Button
                                                variant="green"
                                                className={`mx-05 w-fit-content py-20 px-40 size-16 weight-500`}
                                                isLoading={isSubmitting}
                                                onClick={formRef.current ? formRef.current.handleSubmit(handleSave) : null}
                                                disabled={isSubmitting || selectedObject?.isArchived}
                                            >
                                                Сохранить
                                            </Button>
                                            :
                                            <Button
                                                variant="green"
                                                className={`mx-05 py-20 px-20 size-16 weight-500`}
                                                isLoading={isSubmitting}
                                                onClick={e => handleArchive(false)}
                                                disabled={isSubmitting}
                                            >
                                                Разархивировать
                                            </Button>
                                        }
                                    </div>
                                </div>
                                :
                                <div className="footer w-100 d-flex justify-content-between align-items-center mt-0">
                                    <div>

                                    </div>

                                    <div className="d-flex">
                                        <Button
                                            variant="green"
                                            className={`mx-05 w-fit-content py-20 px-40 size-16 weight-500`}
                                            isLoading={isHistorySubmitting}
                                            onClick={saveHistoryEntry}
                                            disabled={isHistorySubmitting}
                                        >
                                            Сохранить эту версию
                                        </Button>
                                    </div>
                                </div>}

                            <div className="w-100 d-flex flex-column mt-50">
                                <div className="tabs d-flex">
                                    {logSections.map((section) => {
                                        return (
                                            <div
                                                key={section.name}
                                                className={`btn style-default ${selectedSectionName == section.name
                                                    ? "style-green"
                                                    : ""
                                                    }`}
                                                onMouseDown={() => selectSection(section.name)}
                                            >
                                                {section.title}
                                            </div>
                                        );
                                    })}
                                </div>

                                <div className="" style={{ marginTop: '25px' }}>
                                    {logSections.map((section) =>
                                        <div key={section.name} className={`${section.name == selectedSectionName ? 'd-block' : 'd-none'}`}> {section.block} </div>
                                    )}

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div >

            <UnarchiveModal ref={unarchiveModalRef} objectName={selectedObject?.keyString} />
            <ArchiveModal ref={archiveModalRef} objectName={selectedObject?.keyString} />
        </>
    )
}