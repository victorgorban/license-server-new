//* секция Библиотеки c функциями
import * as React from "react";
import ReactDOM from "react-dom";
import _ from 'lodash'
import debounce from 'debounce-promise'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers'
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Link from 'next/link'

//* endof  Компоненты из библиотек

//* секция Наши компоненты
import TemplateTable from '@components/_global/TemplateTable'
//* endof  Наши компоненты

export default function Component({ selectedObject }) {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения


    //* контекст
    //* endof контекст

    //* состояние

    //* endof состояние

    //* вычисляемые переменные, изменение состояния
    //* endof вычисляемые переменные, изменение состояния
    //* endof эффекты

    //* функции-хелперы, НЕ ОБРАБОТЧИКИ
    function deviceValidUntil_string(device) {
        if (selectedObject.deactivationMethod == 'perDevice') {
            return commonHelpers.formatDate(device.validUntil) || 'Не указано'
        } else if (selectedObject.deactivationMethod== 'validUntil'){
            return commonHelpers.formatDate(selectedObject.validUntil) || 'Не указано'
        } else return 'Не применимо'
    }
    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики

    //* endof обработчики

    //* данные для TemplateTable
    let tableHeaders = [
        {
            key: "activationDate",
            isSortable: true,
            title: "Дата активации"
        },
        {
            key: "validUntil",
            isSortable: false,
            title: "Дата окончания лицензии"
        },
        {
            key: "deviceInfo.system",
            isSortable: false,
            title: "Система"
        },
        {
            key: "deviceInfo.cpu",
            isSortable: false,
            title: "Процессор"
        }
    ];

    let tableFields = React.useMemo(() => [
        {
            render: (item, idx) =>
                <td className="td name">
                    {commonHelpers.formatDate(item.activationDate)}
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {deviceValidUntil_string(item)}
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {item.deviceInfo?.system}
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {item.deviceInfo?.cpu}
                </td>
        },
    ])

    let tableActions = React.useMemo(() => function TableActions(row, idx) {
        return (
            <>
            </>
        );
    }, [])
    //* endof данные для TemplateTable

    return (
        <>
            <div className="w-100 pb-20">
                <div className="d-flex justify-content-between w-100 w-100">
                    <div className="w-100 pt-20 d-flex flex-column align-items-center form">
                        <div className="w-100 bg-white py-10 d-flex flex-column align-items-center form">

                            <TemplateTable
                                edit={false}
                                headers={tableHeaders}
                                fields={tableFields}
                                rows={selectedObject?.devices || []}
                                keyField="signature"
                                emptyField="signature"
                                isAddEmptyTableItem={false}
                                showByDefault={true}
                                isActions={false}
                                actions={tableActions}
                                isBatchActions={false}
                                isRefreshTable={false}
                                tableClasses="table w-100"
                                pageSizeDefault={10} // 999 - значит пагинация нестандартная, контролируется вне TemplateTable
                                isBottomPages={true}
                            />
                        </div>
                    </div>

                </div>
            </div>
        </>
    )
}