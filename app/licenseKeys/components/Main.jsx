//* секция Библиотеки c функциями
import * as React from "react";
import ReactDOM from "react-dom";
import _ from 'lodash'
import debounce from 'debounce-promise'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers'
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
import { ReactContext as PageDataContext } from '@components/providers/PageData'
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Link from 'next/link'
import { useRouter, usePathname } from 'next/navigation';

//* endof  Компоненты из библиотек

//* секция Наши компоненты
// import EditObjectModal from './EditObjectModal_old'
import CreateObjectModal from './CreateObjectModal'
import UnarchiveModal from '@components/_global/UnarchiveModal'
import TemplateTable from '@components/_global/TemplateTable'
import CustomTableFooter from '@components/_global/CustomTableFooter'
import CustomTableHeader from '@components/_global/CustomTableHeader'
//* endof  Наши компоненты

export default function Main() {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения


    //* контекст
    let { state: pageData } = React.useContext(PageDataContext);
    //* endof контекст

    //* состояние
    let [newLicenseKey, setNewLicenseKey] = React.useState(null)
    let [defaultCompaniesOptions, setDefaultCompaniesOptions] = React.useState()
    let [defaultUsersOptions, setDefaultUsersOptions] = React.useState()
    let [defaultProjectsOptions, setDefaultProjectsOptions] = React.useState()
    console.log('defaultProjectsOptions', defaultProjectsOptions)

    const unarchiveModalRef = React.useRef();
    // const editLicenseKeyModalRef = React.useRef();
    const createLicenseKeyModalRef = React.useRef();

    let [tableSettings, setTableSettings] = React.useState(_.pick(pageData, ['pageNumbersToShow', 'dataLength', 'firstPage', 'lastPage', 'selectedPage', 'pageRows', 'pageSize', 'isArchived']))
    const [isTableRefreshing, setTableRefreshing] = React.useState(false)
    //* endof состояние

    //* вычисляемые переменные, изменение состояния
    let { pageNumbersToShow, firstPage, lastPage, selectedPage, pageRows, pageSize, dataLength, isArchived } = tableSettings;
    //* endof вычисляемые переменные, изменение состояния

    React.useEffect(() => {
        clientHelpers.loadOptions.loadCompaniesOptions('', setDefaultCompaniesOptions)
        clientHelpers.loadOptions.loadProjectsOptions('', setDefaultProjectsOptions)
        clientHelpers.loadOptions.loadUsersOptions('', setDefaultUsersOptions)
    }, [])
    //* endof эффекты

    //* функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики
    const debounceHandleInputSearch = debounce((e) => {
        let inputValue = e.target.value;
        tableSettings.search = inputValue || '';
        refreshTable();
    }, 300)

    async function handleSetPage(value) {
        tableSettings.selectedPage = value;
        await refreshTable()
    }

    async function refreshTable() {
        try {
            setTableRefreshing(true)
            let tableSettingsToSend = _.pick(tableSettings, ['filter', 'search', 'sortConfig', 'selectedPage', 'pageSize', 'projection', 'isArchived'])
            let newTableSettings = await clientHelpers.submitObject("/api/licenseKeys/tableFilterAndSort", tableSettingsToSend);
            setTableSettings({ ...tableSettings, ...newTableSettings })
        } catch (e) {
            console.log('Ошибка в RefreshTable', e.message)
            notifications.showSystemError(e.message)
        } finally {
            setTableRefreshing(false)
        }
    }

    async function handleSetPageSize(value) {
        tableSettings.pageSize = value;
        await refreshTable()
    }

    async function handleRequestsSortCustom(field, direction) {
        tableSettings.sortConfig = { field, direction };
        await refreshTable()
    }

    async function handleSelectNewLicenseKey(key) {
        setNewLicenseKey(key)

        createLicenseKeyModalRef.current.setModalOpen(true)
        // в случае с модалкой тут просто: привязать обработчик архивации, показать модалку с selectedObject. Я считаю, отдельную страницу для этого делать может быть непрактично и слишком широко (данных нет толком), если только в случае статистики по рыбе.
    }

    async function handleCreateLicenseKey(fields) {
        if (fields.validUntil) {
            fields.validUntil = new Date(fields.validUntil)
        }

        if (fields.transmissionProtectionAesKey) {
            fields.transmissionProtectionAesKey = JSON.parse(fields.transmissionProtectionAesKey)
        }

        let { publicDataUrl } = await clientHelpers.submitObject('/api/licenseKeys/create', fields, { timeout: 10_000 })
        refreshTable();
        clientHelpers.download(publicDataUrl, 'publicKey.json');
        notifications.showSuccess('Лиц. ключ создан')
    }
    //* endof обработчики

    //* данные для TemplateTable
    let tableHeaders = [
        {
            key: "keyString",
            isSortable: true,
            title: "Строковое значение"
        },
        {
            key: "projectAppId",
            isSortable: true,
            title: "Проект"
        },
        {
            key: "activationScope",
            isSortable: true,
            title: "Область активации"
        },
        {
            key: "protectionType",
            isSortable: true,
            title: "Защита ключа"
        },
        {
            key: "maxDevices",
            isSortable: true,
            title: "Устройств"
        },
    ];

    let tableFields = React.useMemo(() => [
        {
            render: (item, idx) =>
                <td className="td name">
                    <Link href={`/licenseKeys/${item._id}`}
                        className="link"
                    >
                        {item.keyString}{item.isArchived && ' (Архивирован)'}
                    </Link>
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {item.projectAppId || 'Нет данных'}
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {commonHelpers.licenseKeys.activationScopesForSelect.find(obj => obj.value == item.activationScope)?.label || 'Нет данных'}
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {item.protectionType == 'none' ? 'Без защиты' : item.protectionType}
                </td>
        },
        {
            render: (item, idx) =>
                <td className="td name">
                    {item.maxDevices ? `${item.activations?.length || 0} из ${item.maxDevices}` : 'Без ограничений'}
                </td>
        },
    ])

    let tableActions = React.useMemo(() => function TableActions(row, idx) {
        return (
            <>
            </>
        );
    }, [])
    //* endof данные для TemplateTable

    return (
        <>
            <div className="w-100 pb-20 px-20">
                <div className="d-flex justify-content-between w-100 w-100">
                    <div className="w-100 pt-20 d-flex flex-column align-items-center form">
                        <CustomTableHeader {...{ tableSettings, refreshTable, isTableRefreshing, isSearch: true, handleInputSearch: debounceHandleInputSearch, dataLength, text: 'Лиц. ключи', handleSelectNewObject: handleSelectNewLicenseKey }} />

                        <div className="w-100 bg-white py-10 px-20 d-flex flex-column align-items-center form">

                            <TemplateTable
                                edit={false}
                                headers={tableHeaders}
                                fields={tableFields}
                                rows={pageRows || []}
                                keyField="_id"
                                emptyField="_id"
                                isAddEmptyTableItem={false}
                                showByDefault={true}
                                isActions={false}
                                actions={tableActions}
                                isBatchActions={false}
                                isRefreshTable={false}
                                isCustomSort={true}
                                requestSortCustom={handleRequestsSortCustom}
                                tableClasses="table w-100"
                                pageSizeDefault={999} // 999 - значит пагинация нестандартная, контролируется вне TemplateTable
                                isTopPages={false}
                                isBottomPages={false}
                            />

                            <CustomTableFooter
                                {...
                                {
                                    selectedPage,
                                    firstPage,
                                    lastPage,
                                    isTableRefreshing,
                                    handleSetPage,
                                    pageNumbersToShow,
                                    pageSize,
                                    handleSetPageSize
                                }}
                            />
                        </div>
                    </div>

                </div>
            </div>

            <UnarchiveModal ref={unarchiveModalRef} />
            <CreateObjectModal {...{ defaultCompaniesOptions, defaultUsersOptions, defaultProjectsOptions, newLicenseKey }} onCreateLicenseKey={handleCreateLicenseKey} ref={createLicenseKeyModalRef} />
        </>
    )
}