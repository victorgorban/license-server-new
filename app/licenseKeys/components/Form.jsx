'use client'
//* секция Библиотеки c функциями
import React from 'react';
import dayjs from "dayjs";
import _ from 'lodash';

//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers'
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import { Controller, useForm } from 'react-hook-form';
//* endof  Компоненты из библиотек

//* секция Наши компоненты
import Button from '@components/animated/Button'
import RoundIcon from '@components/wrappers/RoundIcon'
import Tooltip from '@components/_global/Tooltip'
import * as formComponents from '@components/forms'
import * as licenseKeysHelpers from '@root/helpers/client/loadOptions'
//* endof  Наши компоненты

//* секция Стили компонента
//* endof  Стили компонента


export default React.forwardRef(function Component({ isDisabled, disabledFields = [], mode = "create", selectedObject, defaultCompaniesOptions = [], defaultUsersOptions = [], defaultProjectsOptions = []}, elRef) {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения

    //* контекст
    //* endof контекст


    //* состояние
    const [selectedProtectionType, setSelectedProtectionType] = React.useState()
    const [selectedActivationScope, setSelectedActivationScope] = React.useState()
    const [selectedDeactivationMethod, setSelectedDeactivationMethod] = React.useState()
    const [selectedCompany, setSelectedCompany] = React.useState()
    const [selectedProject, setSelectedProject] = React.useState()
    const [selectedUser, setSelectedUser] = React.useState()

    const [current_formState, setCurrent_formState] = React.useState({});

    const {
        control,
        handleSubmit,
        reset,
        setValue
    } = useForm({
        mode: 'onTouched'
    });
    //* endof состояние

    //* секция вычисляемые переменные, изменение состояния

    let protectionTypesForSelect = commonHelpers.licenseKeys.protectionTypesForSelect;
    let statusesForSelect = commonHelpers.licenseKeys.statusesForSelect;
    let activationScopesForSelect = commonHelpers.licenseKeys.activationScopesForSelect;
    let deactivationMethodsForSelect = commonHelpers.licenseKeys.deactivationMethodsForSelect;

    let companiesForSelect = _.cloneDeep(defaultCompaniesOptions);
    if (selectedCompany && !defaultCompaniesOptions.find(option => option.value == selectedCompany._id)) {
        companiesForSelect.unshift({ value: selectedCompany._id, label: commonHelpers.formatCompanyName(selectedCompany) })
    }

    let usersForSelect = _.cloneDeep(defaultUsersOptions);
    if (selectedUser && !defaultUsersOptions.find(option => option.value == selectedUser._id)) {
        usersForSelect.unshift({ value: selectedUser._id, label: selectedUser.name })
    }

    let projectsForSelect = _.cloneDeep(defaultProjectsOptions);
    if (selectedProject && !defaultProjectsOptions.find(option => option.value == selectedProject._id)) {
        projectsForSelect.unshift({ value: selectedProject._id, label: selectedProject.name })
    }

    //* endof вычисляемые переменные, изменение состояния

    //* эффекты

    // Форма создается и уходит каждый раз при открытии модалки
    React.useEffect(() => {
        performReset(getDefaultValues());
    }, [selectedObject?._id])

    React.useImperativeHandle(elRef, () => ({
        performReset(newObj) {
            performReset(newObj || getDefaultValues())
        },
        getFormState() {
            return current_formState;
        },
        handleSubmit
    }))

    //* endof эффекты

    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ
    function getDefaultValues() {
        if (!selectedObject) return {}
        // я использую перечисление, потому это удобно (видны все используемые поля). К тому же очень часто поля формы не соответствуют полям конечного объекта.
        let keys = ['keyString', 'protectionType', 'activationScope', 'deactivationMethod', 'deviceValidPeriod',
            'maxDevices', 'projectId', 'companyId', 'userId', 'status', 'validUntil', 'transmissionProtectionAesKey']
        let result = {}
        for (let key of keys) {
            result[key] = selectedObject[key]
        }
        if (result.validUntil) {
            result.validUntil = commonHelpers.formatDate(result.validUntil, 'YYYY-MM-DD')
        }
        if (result.transmissionProtectionAesKey) {
            result.transmissionProtectionAesKey = JSON.stringify(result.transmissionProtectionAesKey)
        }

        return result;
    }

    function performReset(newValues) {
        setCurrent_formState(_.cloneDeep(newValues))
        setSelectedProtectionType(newValues.protectionType);
        setSelectedActivationScope(newValues.activationScope);
        setSelectedDeactivationMethod(newValues.deactivationMethod);
        clientHelpers.loadOptions.loadCompanyById(newValues.companyId, setSelectedCompany);
        clientHelpers.loadOptions.loadProjectById(newValues.projectId, setSelectedProject);
        reset(_.cloneDeep(newValues));
    }
    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики
    function generateKeyString() {
        let keyString = commonHelpers.randomIntString(16);
        keyString = `${keyString.substring(0, 4)}-${keyString.substring(4, 8)}-${keyString.substring(8, 12)}-${keyString.substring(12, 16)}`
        current_formState.keyString = keyString;
        setValue('keyString', keyString, { shouldValidate: true })
    }

    function addValidUntil(amount = 1, measure = 'month') {
        current_formState.validUntil = dayjs(current_formState.validUntil).add(amount, measure).format('YYYY-MM-DD');
        setValue('validUntil', current_formState.validUntil, { shouldValidate: true })
    }

    function addDeviceValidPeriod(amount = 1, measure = 'month') {
        let days = 0;
        if (measure.includes('day')) {
            days = amount;
        } else if (measure.includes('week')) {
            days = amount * 7;
        } else if (measure.includes('month')) {
            days = amount * 30;
        } else if (measure.includes('year')) {
            days = amount * 365;
        }

        current_formState.deviceValidPeriod = (+current_formState.deviceValidPeriod || 0) + days;
        setValue('deviceValidPeriod', current_formState.deviceValidPeriod, { shouldValidate: true })
    }

    function addMaxDevices(amount = 1) {
        current_formState.maxDevices = (+current_formState.maxDevices || 0) + amount;
        setValue('maxDevices', current_formState.maxDevices, { shouldValidate: true })
    }

    //* endof обработчики

    return (
        <div className="content form d-flex flex-wrap pt-30 pb-40">
            {(() => {
                let fieldName = 'keyString'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: "Поле обязательно",
                            validate(v) {
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Строковое значение*</span>

                                <div className="d-flex justify-content-between w-100">
                                    <div className="input-wrapper" style={{ width: 'calc(100% - 40px)' }}>
                                        <formComponents.TextInput
                                            {...field}
                                            disabled={isDisabledField}
                                            value={field.value || ''}
                                            onChange={(e) => {
                                                field.onChange(e);
                                                clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                            }}
                                        />
                                    </div>

                                    <Tooltip
                                        placement="top"
                                        content="Сгенерировать"
                                    >
                                        <RoundIcon
                                            variant="button"
                                            color="brand1"
                                            iconName="lightbulb-o"
                                            disabled={isDisabledField}
                                            onClick={generateKeyString} />
                                    </Tooltip>
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'deactivationMethod'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: "Поле обязательно",
                            validate(v) {
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Способ отключения*</span>

                                <div className="d-flex justify-content-between w-100">
                                    <div className="input-wrapper" style={{ width: 'calc(100% - 40px)' }}>
                                        <formComponents.SelectInput
                                            {...field}
                                            isDisabled={isDisabledField}
                                            value={deactivationMethodsForSelect.find(sfs => sfs.value == field.value)}
                                            onChange={(option, config) => {
                                                field.onChange(option.value);
                                                setSelectedDeactivationMethod(option.value);
                                                clientHelpers.handleChangeItemSelect(option, current_formState, fieldName);
                                            }}
                                            noOptionsMessage={() => 'Ничего не найдено...'}
                                            loadOptions={(inputValue, callback) => {
                                                callback(deactivationMethodsForSelect.filter(x => x.label.toLowerCase().includes(inputValue.toLowerCase())).slice(0, 50))
                                            }}
                                            defaultOptions={deactivationMethodsForSelect.slice(0, 50)}
                                            isSearchable={deactivationMethodsForSelect.length > 7}
                                        />
                                    </div>

                                    <Tooltip
                                        placement="top"
                                        content={commonHelpers.licenseKeys.deactivationMethodTooltip(selectedDeactivationMethod)}
                                    >
                                        <RoundIcon color="brand1" iconName="info" />
                                    </Tooltip>
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'validUntil'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return selectedDeactivationMethod == 'validUntil' ? <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            validate(v) {
                                // if (v && new Date(v) < new Date()) {
                                //     return 'Дата не может быть меньше текущей'
                                // }
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Действует до: </span>

                                <div className="input-wrapper">
                                    <formComponents.DateInput
                                        {...field}
                                        disabled={isDisabledField}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            if (e.currentTarget.value) { // invalid date в onChange пустая вообще. А с Invalid Date не будет работать добавление дат.
                                                clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                            }
                                        }}
                                    />

                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />

                    <div className="form-group flex-row w-md-50 align-items-end">
                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addValidUntil(30, 'days')}
                            >
                                +30 д.
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addValidUntil(90, 'days')}
                            >
                                +90 д.
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addValidUntil(6, 'month')}
                            >
                                +6 мес.
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addValidUntil(1, 'year')}
                            >
                                +1 год
                            </Button>
                        </div>
                    </div>
                </>
                    : null
            })()}

            {(() => {
                let fieldName = 'deviceValidPeriod'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return selectedDeactivationMethod == 'perDevice' ? <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: "Поле обязательно",
                            validate(v) {
                                if (v && v.length && v <= 0) {
                                    return 'Требуется положительное число дней'
                                }
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Дней использования устройства: *</span>

                                <div className="input-wrapper">
                                    <formComponents.NumberInput
                                        {...field}
                                        disabled={isDisabledField}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                        }}
                                    />

                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />

                    <div className="form-group flex-row w-md-50 align-items-end">
                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addDeviceValidPeriod(7, 'days')}
                            >
                                +1 нед.
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addDeviceValidPeriod(14, 'days')}
                            >
                                +2 нед.
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addDeviceValidPeriod(30, 'days')}
                            >
                                +1 мес.
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addDeviceValidPeriod(6, 'month')}
                            >
                                +6 мес.
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addDeviceValidPeriod(1, 'year')}
                            >
                                +1 год
                            </Button>
                        </div>
                    </div>
                </>
                    : null
            })()}

            {(() => {
                let fieldName = 'status'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: "Поле обязательно",
                            validate(v) {
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Статус*</span>

                                <div className="input-wrapper">
                                    <formComponents.SelectInput
                                        {...field}
                                        isDisabled={isDisabledField}
                                        value={statusesForSelect.find(sfs => sfs.value == field.value)}
                                        onChange={(option, config) => {
                                            field.onChange(option.value);
                                            clientHelpers.handleChangeItemSelect(option, current_formState, fieldName);
                                        }}
                                        noOptionsMessage={() => 'Ничего не найдено...'}
                                        loadOptions={(inputValue, callback) => {
                                            callback(statusesForSelect.filter(x => x.label.toLowerCase().includes(inputValue.toLowerCase())).slice(0, 50))
                                        }}
                                        defaultOptions={statusesForSelect.slice(0, 50)}
                                        isSearchable={statusesForSelect.length > 7}
                                    />
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}


            {(() => {
                let fieldName = 'projectId'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: 'Укажите проект',
                            validate(v) {
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Проект *</span>

                                <formComponents.SelectInput
                                    onChange={(option, config) => {
                                        field.onChange(option.value);
                                        clientHelpers.loadOptions.loadProjectById(option.value, setSelectedProject);
                                        clientHelpers.handleChangeItemSelect(option, current_formState, fieldName);
                                    }}
                                    isDisabled={isDisabledField}
                                    noOptionsMessage={() => 'Ничего не найдено...'}
                                    value={projectsForSelect.find(sfs => sfs.value == field.value)}
                                    loadOptions={clientHelpers.loadOptions.debounceLoadProjectsOptions}
                                    defaultOptions={projectsForSelect}
                                    isSearchable={true}
                                />
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'maxDevices'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            validate(v) {
                                // if (v && new Date(v) < new Date()) {
                                //     return 'Дата не может быть меньше текущей'
                                // }
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Число устройств: </span>

                                <div className="input-wrapper">
                                    <formComponents.NumberInput
                                        {...field}
                                        disabled={isDisabledField}
                                        value={field.value || ''}
                                        onChange={(e) => {
                                            field.onChange(e);
                                            clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                        }}
                                        placeholder="0 - неограниченно"
                                    />

                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />

                    <div className="form-group flex-row w-md-50 align-items-end">
                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addMaxDevices(1)}
                            >
                                +1
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addMaxDevices(5)}
                            >
                                +5
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addMaxDevices(10)}
                            >
                                +10
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addMaxDevices(25)}
                            >
                                +25
                            </Button>
                        </div>

                        <div className="px-05">
                            <Button
                                variant="green"
                                className={`w-fit-content`}
                                disabled={isDisabledField}
                                onClick={e => addMaxDevices(50)}
                            >
                                +50
                            </Button>
                        </div>
                    </div>
                </>
            })()}

            {(() => {
                let fieldName = 'activationScope'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: "Поле обязательно",
                            validate(v) {
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Область действия*</span>

                                <div className="d-flex justify-content-between w-100">
                                    <div className="input-wrapper" style={{ width: 'calc(100% - 40px)' }}>
                                        <formComponents.SelectInput
                                            {...field}
                                            isDisabled={isDisabledField}
                                            value={activationScopesForSelect.find(sfs => sfs.value == field.value)}
                                            onChange={(option, config) => {
                                                field.onChange(option.value);
                                                setSelectedActivationScope(option.value);
                                                clientHelpers.handleChangeItemSelect(option, current_formState, fieldName);
                                            }}
                                            noOptionsMessage={() => 'Ничего не найдено...'}
                                            loadOptions={(inputValue, callback) => {
                                                callback(activationScopesForSelect.filter(x => x.label.toLowerCase().includes(inputValue.toLowerCase())).slice(0, 50))
                                            }}
                                            defaultOptions={activationScopesForSelect.slice(0, 50)}
                                            isSearchable={activationScopesForSelect.length > 7}
                                        />
                                    </div>

                                    <Tooltip
                                        placement="top"
                                        content={commonHelpers.licenseKeys.activationScopeTooltip(selectedActivationScope)}
                                    >
                                        <RoundIcon color="brand1" iconName="info" />
                                    </Tooltip>
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}


            {selectedActivationScope == 'company' ?
                (() => {
                    let fieldName = 'companyId'
                    let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                    return <>
                        <Controller
                            control={control}
                            name={fieldName}
                            rules={{
                                required: 'Укажите компанию',
                                validate(v) {
                                    return true;
                                }
                            }}
                            render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                                <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                    <span className="label weight-600 size-14">Компания *</span>

                                    <formComponents.SelectInput
                                        onChange={(option, config) => {
                                            field.onChange(option.value);
                                            clientHelpers.loadOptions.loadCompanyById(option.value, setSelectedCompany);
                                            clientHelpers.handleChangeItemSelect(option, current_formState, fieldName);
                                        }}
                                        isDisabled={isDisabledField}
                                        noOptionsMessage={() => 'Ничего не найдено...'}
                                        value={companiesForSelect.find(sfs => sfs.value == field.value)}
                                        loadOptions={clientHelpers.loadOptions.debounceLoadCompaniesOptions}
                                        defaultOptions={companiesForSelect}
                                        isSearchable={true}
                                    />

                                    <span className="field-error">{error?.message}</span>
                                </label>
                            )}
                        />
                    </>
                })()
                : null}

            {selectedActivationScope == 'personal' ?
                (() => {
                    let fieldName = 'userId'
                    let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                    return <>
                        <Controller
                            control={control}
                            name={fieldName}
                            rules={{
                                required: 'Укажите пользователя',
                                validate(v) {
                                    return true;
                                }
                            }}
                            render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                                <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                    <span className="label weight-600 size-14">Пользователь *</span>

                                    <formComponents.SelectInput
                                        onChange={(option, config) => {
                                            field.onChange(option.value);
                                            clientHelpers.loadOptions.loadUserById(option.value, setSelectedUser);
                                            clientHelpers.handleChangeItemSelect(option, current_formState, fieldName);
                                        }}
                                        isDisabled={isDisabledField}
                                        noOptionsMessage={() => 'Ничего не найдено...'}
                                        value={usersForSelect.find(sfs => sfs.value == field.value)}
                                        loadOptions={clientHelpers.loadOptions.debounceLoadUsersOptions}
                                        defaultOptions={usersForSelect}
                                        isSearchable={true}
                                    />

                                    <span className="field-error">{error?.message}</span>
                                </label>
                            )}
                        />
                    </>
                })()
                : null}

            {(() => {
                let fieldName = 'protectionType'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: "Поле обязательно",
                            validate(v) {
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Защита ключа*</span>

                                <div className="d-flex justify-content-between w-100">
                                    <div className="input-wrapper" style={{ width: 'calc(100% - 40px)' }}>
                                        <formComponents.SelectInput
                                            {...field}
                                            value={protectionTypesForSelect.find(sfs => sfs.value == field.value)}
                                            isDisabled={isDisabledField}
                                            onChange={(option, config) => {
                                                field.onChange(option.value);
                                                setSelectedProtectionType(option.value);
                                                clientHelpers.handleChangeItemSelect(option, current_formState, fieldName);
                                            }}
                                            loadOptions={(inputValue, callback) => {
                                                callback(protectionTypesForSelect.filter(x => x.label.toLowerCase().includes(inputValue.toLowerCase())).slice(0, 50))
                                            }}
                                            defaultOptions={protectionTypesForSelect.slice(0, 50)}
                                            isSearchable={protectionTypesForSelect.length > 7}
                                        />
                                    </div>

                                    <Tooltip
                                        placement="top"
                                        content={commonHelpers.licenseKeys.protectionTypeTooltip(selectedProtectionType)}
                                    >
                                        <RoundIcon color="brand1" iconName="info" />
                                    </Tooltip>
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />
                </>
            })()}

            {(() => {
                let fieldName = 'transmissionProtectionAesKey'
                let isDisabledField = isDisabled || disabledFields.includes(fieldName);
                return <>
                    <Controller
                        control={control}
                        name={fieldName}
                        rules={{
                            required: 'Поле обязательно',
                            validate(v) {
                                if (v) {
                                    let parsedValue;
                                    try {
                                        parsedValue = JSON.parse(v);
                                        if (!Array.isArray(parsedValue)) throw new Error('Не массив')
                                        if (parsedValue.length < 16) return 'Требуется массив с длиной минимум 16'
                                        for (let entry of parsedValue) {
                                            if (entry < 0 || entry > 255) {
                                                throw new Error('Не байт')
                                            }
                                        }
                                    } catch (e) {
                                        return 'Массив должен быть массивом байтов'
                                    }
                                }
                                return true;
                            }
                        }}
                        render={({ field, fieldState: { invalid, isTouched, isDirty, error }, formState }) => (
                            <label className={`form-group w-md-50 ${invalid ? 'error' : ''}`}>
                                <span className="label weight-600 size-14">Защита передачи *</span>

                                <div className="d-flex justify-content-between w-100">
                                    <div className="input-wrapper" style={{ width: 'calc(100% - 40px)' }}>
                                        <formComponents.TextAreaInput
                                            {...field}
                                            disabled={isDisabledField}
                                            value={field.value || ''}
                                            onChange={(e) => {
                                                field.onChange(e);
                                                clientHelpers.handleChangeItem(e, current_formState, fieldName);
                                            }}
                                        />
                                    </div>

                                    <Tooltip
                                        placement="auto"
                                        content={`Массив байтов с длиной 24. Используется при шифровании приема-передачи при активации.
                                        Этим массивом должны шифроваться передаваемые данные как на сервере, так и на клиенте.
                                        Пустое значение означает отсутствие шифрования передачи - крайне не рекомендуется, т.к. это означает уязвимость для прослушки сети.
                                        `}
                                    >
                                        <RoundIcon color="brand1" iconName="info" />
                                    </Tooltip>
                                </div>
                                <span className="field-error">{error?.message}</span>
                            </label>
                        )}
                    />

                </>
            })()}

        </div>
    )
})