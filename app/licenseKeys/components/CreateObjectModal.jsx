'use client'
//* секция Библиотеки c функциями
import React from 'react';
import dayjs from "dayjs";
import _ from 'lodash';

//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as commonHelpers from '@commonHelpers'
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Modal from 'react-modal';
//* endof  Компоненты из библиотек

//* секция Наши компоненты
import Button from '@components/animated/Button'
import Form from './Form'

//* endof  Наши компоненты

//* секция Стили компонента
//* endof  Стили компонента

Modal.setAppElement('#pageRoot');

export default React.forwardRef(function Component({ ...componentParams }, elRef) {
    //* библиотеки и неизменяемые значения
    //* endof библиотеки и неизменяемые значения

    //* контекст
    //* endof контекст


    //* состояние
    const [isModalOpen, setModalOpen] = React.useState(false)
    const [isSubmitting, setSubmitting] = React.useState(false)
    const [isComponentUpdate, setComponentUpdate] = React.useState(false)
    const formRef = React.useRef();
    //* endof состояние

    //* секция вычисляемые переменные, изменение состояния

    //* endof вычисляемые переменные, изменение состояния

    //* эффекты

    React.useImperativeHandle(elRef, () => ({
        setModalOpen,
    }))

    React.useEffect(() => {
        if (isModalOpen) {
            setComponentUpdate(value => !value); // просто чтобы обновился интерфейс и правильно поставился formRef.current
        }
    }, [isModalOpen])

    //* endof эффекты

    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ
    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* обработчики
    function handleCancel() {
        setModalOpen(false)
    }

    async function handleSave() {
        try {
            setSubmitting(true);
            let current_formState = formRef.current.getFormState()
            let objectToSend = _.cloneDeep(current_formState)

            await componentParams.onCreateLicenseKey?.(objectToSend)
            setModalOpen(false)
        } catch (e) {
            notifications.showSystemError(e.message)
        } finally {
            setSubmitting(false);
        }

    }

    //* endof обработчики

    return (
        <Modal
            isOpen={isModalOpen}
            onRequestClose={handleCancel}
            overlayClassName={{
                base: 'modal-overlay-base',
                afterOpen: 'modal-overlay-after',
                beforeClose: 'modal-overlay-before'
            }}
            className={{
                base: 'modal-content-base w-100',
                afterOpen: 'modal-content-after',
                beforeClose: 'modal-content-before'
            }}
            style={{ content: { maxWidth: 1000 } }}
            closeTimeoutMS={500}
        >
            <div className="w-100 h-100 bg-brand5 p-10">
                <div className="w-100 h-100 bg-brand4 position-relative">
                    <Button
                        variant="modal-close"
                        onClick={handleCancel}
                    >
                        <i className="fa fa-close size-36"></i>
                    </Button>
                    <div className="bg-white px-40 py-30 border-radius-5">
                        <div className="header d-flex align-items-center justify-content-center mb-10">
                            <h5 className="modal-heading">
                                Создание лицензионного ключа
                            </h5>
                        </div>

                        <Form ref={formRef} selectedObject={componentParams.newLicenseKey} {...componentParams} />

                        <div className="footer d-flex justify-content-between align-items-center mt-30">
                            <div>

                            </div>

                            <div>
                                <Button
                                    variant="green"
                                    className={`mx-05 py-20 px-20 size-16 weight-500`}
                                    isLoading={isSubmitting}
                                    onClick={formRef.current ? formRef.current.handleSubmit(handleSave) : null}
                                    disabled={isSubmitting}
                                >
                                    Сохранить
                                </Button>

                                <Button
                                    variant="default"
                                    className={`mx-05 py-20 px-20 size-16 weight-500`}
                                    onClick={handleCancel}
                                >
                                    Закрыть это окно
                                </Button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    )
})