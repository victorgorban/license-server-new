//* секция Библиотеки c функциями
import React from "react";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
import SelectInput from '@components/forms/SelectInput'

//* endof  Наши компоненты

export default function Component({
    selectedPage,
    firstPage,
    lastPage,
    isTableRefreshing,
    handleSetPage,
    pageNumbersToShow,
    pageSize,
    handleSetPageSize
}) {

    //* секция глобальное состояние из context
    //* endof глобальное состояние из context


    //* секция состояние

    //* endof состояние


    //* секция вычисляемые переменные, изменение состояния
    let pageSizesForSelect = [3, 10, 15, 25, 50].map(v => ({ value: v, label: `${v}` }))
    //* endof вычисляемые переменные, изменение состояния


    //* секция эффекты

    //* endof эффекты


    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* секция обработчики
    //* endof обработчики


    return (
        <>
            {/* пагинация */}
            <div className={`w-100 mt-10 d-flex justify-content-center justify-content-lg-end align-items-center`}>
                <div className={`w-100 w-lg-50 d-flex flex-wrap justify-content-center justify-content-lg-end table-pagination numbers `}>
                    <span className={`item bordered ${selectedPage == firstPage && 'disabled'}`} disabled={lastPage == 0} onClick={() => handleSetPage(selectedPage - 1)}>
                        &#60;
                    </span>

                    {lastPage != 0 && lastPage >= 5 && selectedPage >= firstPage + 3 &&
                        <>
                            <span className={`item ${selectedPage == firstPage && 'disabled'}`} disabled={false} onClick={() => handleSetPage(firstPage)}>{firstPage}</span>
                            {selectedPage > firstPage + 3 && <span className="item empty">...</span>}
                        </>
                    }
                    {pageNumbersToShow.map(pageNumber =>
                        <span key={pageNumber} className={`item ${selectedPage == pageNumber && 'active'}`} disabled={false} onClick={() => handleSetPage(pageNumber)}>{pageNumber}</span>
                    )}
                    {lastPage != 0 && lastPage >= 5 && selectedPage <= lastPage - 3 &&
                        <>
                            {selectedPage < lastPage - 3 && <span className="item empty">...</span>}
                            <span className={`item ${selectedPage == lastPage && 'disabled'}`} disabled={false} onClick={() => handleSetPage(lastPage)}>{lastPage}</span>
                        </>
                    }

                    <span className={`item bordered ${selectedPage == lastPage && 'disabled'}`} disabled={lastPage == 0} onClick={() => handleSetPage(selectedPage + 1)}>
                        &#62;
                    </span>

                </div>


                <div className="d-none d-lg-block ml-10" style={{ width: '100px' }}>
                    <SelectInput
                        value={pageSizesForSelect.find(option => option.value == pageSize)}
                        className="table-select"
                        defaultOptions={pageSizesForSelect}
                        isSearchable={false}
                        isDisabled={false}
                        onChange={(option, config) => { handleSetPageSize(option.value) }}
                    />
                </div>
            </div>
        </>
    )

}