//* секция Библиотеки c функциями
import React from "react";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
import Button from '@components/animated/Button'
import AnimatedNumberTicker from '@root/components/animated/NumberTicker'
import * as formComponents from '@components/forms'
//* endof  Наши компоненты

export default function Component({
    tableSettings, refreshTable,
    isTableRefreshing, dataLength,
    TooltipBlock = null,
    isActions = true,
    isSearch = false, handleInputSearch=null,
    text, handleSelectNewObject
}) {
    //* секция глобальное состояние из context
    //* endof глобальное состояние из context

    //* секция состояние
    //* endof состояние


    //* секция вычисляемые переменные, изменение состояния
    let isArchivedForSelect = [
        { value: false, label: 'Актуальные' },
        { value: true, label: 'Архив' }
    ]
    //* endof вычисляемые переменные, изменение состояния


    //* секция эффекты


    //* endof эффекты


    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* секция обработчики
    function handleSetArchived(value) {
        if (value == tableSettings.isArchived) return;
        tableSettings.isArchived = value;
        refreshTable();
    }
    //* endof обработчики

    return (
        <>
            <div className="page-header w-100 mb-20 d-flex justify-content-between color-gray3 ">
                <h2 className={`weight-400 size-21 d-flex align-items-center ${isTableRefreshing && 'loading'}`}>
                    {text}
                    &nbsp;(<AnimatedNumberTicker value={dataLength} variant="short" />)
                    {TooltipBlock && <>
                        <div className="pl-05"></div>
                        <TooltipBlock />
                    </>}
                    <div className="loader lds-ring">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </h2>

                {isSearch &&
                    <formComponents.TextInput
                        onChange={handleInputSearch}
                        style={{minWidth: 300}}
                        placeholder="Поиск: начните вводить..."
                    />
                }

                <div className={`actions d-flex`}>
                    {isActions && <>
                        <div className="mr-10 color-black" style={{ width: '180px' }}>
                            <formComponents.SelectInput
                                value={isArchivedForSelect.find(option => option.value == tableSettings.isArchived)}
                                className="table-select"
                                defaultOptions={isArchivedForSelect}
                                isSearchable={false}
                                isDisabled={false}
                                onChange={(option, config) => { handleSetArchived(option.value) }}
                            />
                        </div>

                        <Button
                            variant="green"
                            className={`btn w-fit-content size-14 px-20`}
                            disabled={false}
                            onClick={e => handleSelectNewObject({})}
                        >
                            <i className="fa fa-plus mr-05"></i> Добавить
                        </Button>
                    </>
                    }
                </div>
            </div>
        </>
    )

}