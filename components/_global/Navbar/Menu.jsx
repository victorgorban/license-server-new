'use client'
//* секция Библиотеки c функциями
import * as React from "react";
import _ from 'lodash'
import EJSON from 'ejson'
import { useRouter, usePathname } from 'next/navigation'
import { motion, usePresence, AnimatePresence } from "framer-motion";
import { useStore } from 'effector-react'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as clientHelpers from '@clientHelpers'
import * as notifications from '@clientHelpers/notifications'
//* endof  Наши хелперы

//* секция Контекст и store
import { ReactContext as UserContext } from '@components/providers/User'
import { $userData } from '@clientHelpers/stores'
//* endof  Контекст и store

//* секция Компоненты из библиотек
import Link from 'next/link'
//* endof  Компоненты из библиотек

//* секция Виджеты
//* endof  Виджеты

//* секция Наши компоненты
//* endof  Наши компоненты

function NavItem({ children, href, pathname, ...otherProps }) {
    const [isPresent, safeToRemove] = usePresence();

    const transition = { type: "tween", duration: 0.2 };

    const animations = {
        layout: true,
        style: {
        },
        initial: "out",
        animate: "in",
        exit: "out",
        variants: {
            in: { height: 'auto', opacity: 1 },
            out: { height: 25, opacity: 0 },
        },
        onAnimationComplete: () => !isPresent && safeToRemove(),
        transition
    };

    // className={`nav-item ${pathname.startsWith(href) && 'active'}`}
    // className={`link ${pathname.startsWith(href) && 'active'}`}
    return <motion.li {...animations} className={`nav-item ${pathname.startsWith(href) && 'active'}`} {...otherProps}>
        <Link href={href} className={`link ${pathname.startsWith(href) && 'active'}`}>
            {children}
        </Link>
    </motion.li>
}

export default function Component({ }) {
    //* секция глобальное состояние из context
    const user = useStore($userData)
    //* endof глобальное состояние из context

    //* секция состояние
    const router = useRouter();
    const pathname = usePathname();
    const [isSidebarOpen, setSidebarOpen] = React.useState(true)
    //* endof состояние


    //* секция вычисляемые переменные, изменение состояния
    //* endof вычисляемые переменные, изменение состояния

    //* секция эффекты

    // React.useEffect(() => {
    //     setSidebarOpen(true)
    //     document.body.classList.add('sidebar-open')
    // }, [pathname])

    //* endof эффекты

    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ
    function hideSidebar(e) {
        e?.preventDefault();
        setSidebarOpen(false)
        document.body.classList.remove('sidebar-open')
    }

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* секция обработчики
    function openSidebar(e) {
        e?.preventDefault();
        setSidebarOpen(true)
        document.body.classList.add('sidebar-open')
    }

    function toggleSidebar(e) {
        e?.preventDefault();
        isSidebarOpen ? hideSidebar(e) : openSidebar(e)
    }

    async function logout(e) {
        e?.preventDefault();
        try {
            let result = await clientHelpers.submitObject(
                '/api/users/logout',
                {},
            );

            notifications.showSuccess('Вы вышли из системы.')
            $userData.api.reset()
            router.push('/login')
        } catch (e) {
            notifications.showSystemError(e.message);
            console.error(e);
        } finally {
        }
    }

    //* endof обработчики
    return (
        <>
            <nav
                className={`navbar p-0`}
            >
                {/* Блок правого меню, который выезжает при клике на кнопку меню */}
                <div id="rightNav" className={`right-menu-wrapper d-flex ${isSidebarOpen && 'open'}`}
                >
                    <div className="menu bg-brand1 d-flex flex-column px-0 py-20"
                    >
                        <ul className="pl-0 navbar-nav flex-grow-1">
                            <AnimatePresence>
                                {user?.role == 'globalAdmin' ?
                                    <NavItem pathname={pathname} href={'/companies'} key={'/companies'}>
                                        <i className="fa fa-building size-18 mr-10">
                                        </i>
                                        Компании
                                    </NavItem>
                                    : null}
                                {user?.role == 'globalAdmin' ?
                                    <NavItem pathname={pathname} href={'/projects'} key={'/projects'}>
                                        <i className="fa fa-sitemap size-18 mr-10">
                                        </i>
                                        Проекты
                                    </NavItem>
                                    : null}
                                {user?.role == 'globalAdmin' ?
                                    <NavItem pathname={pathname} href={'/users'} key={'/users'}>
                                        <i className="fa fa-user size-18 mr-10">
                                        </i>
                                        Пользователи
                                    </NavItem>
                                    : null}
                                {user ?
                                    <NavItem pathname={pathname} href={'/licenseKeys'} key={'/licenseKeys'}>
                                        <i className="fa fa-user size-18 mr-10">
                                        </i>
                                        Лицензионные ключи
                                    </NavItem>
                                    : null}
                            </AnimatePresence>
                        </ul>

                        {user ?
                            <div className="company-block w-100 d-flex px-20 color-white">
                                <div className="image user-image d-flex all-center">
                                    <i className="fa fa-user size-30"></i>
                                </div>
                                <div className="links d-flex flex-column justify-content-center">
                                    <div className="name mb-05">{user?.name || user?.username}</div>
                                    <div>
                                        <span className="link ml-20" onClick={logout}><i className="fa fa-sign-out mr-05"></i>Выйти</span>
                                    </div>
                                </div>
                            </div>
                            :
                            <div className="company-block w-100 d-flex px-20 color-white">
                                <div className="image user-image d-flex all-center">
                                    <i className="fa fa-user size-30"></i>
                                </div>
                                <div className="links d-flex flex-column justify-content-center">
                                    <div className="name mb-05"></div>
                                    <div>
                                        <Link href="/login"
                                            className={`link ml-20`}
                                        >
                                            <i className="fa fa-sign-in mr-05"></i>Войти
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        }
                    </div>

                    {/* Кнопка которая открывает боковое меню на мобилках и планшетах */}
                    <div className="btn rightbar-toggler d-flex flex-column justify-content-center h-100 color-gray3" onClick={toggleSidebar}>
                        <i className="fa fa-angle-left mb-30 size-34" alt="" />
                        <i className="fa fa-angle-left mt-30 size-34" alt="" />
                    </div>
                </div>
            </nav >

        </>
    )
}
