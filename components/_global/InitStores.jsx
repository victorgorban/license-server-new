'use client'
//* секция Библиотеки c функциями
import * as React from "react";
import _ from 'lodash'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as clientHelpers from '@clientHelpers'
//* endof  Наши хелперы

//* секция Контекст и store
import { $userData, $isGlobalsLoaded } from '@clientHelpers/stores'
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Виджеты
//* endof  Виджеты

//* секция Наши компоненты
//* endof  Наши компоненты

async function setIntervalAndExecute(fn, t) {
    // Error в асинхронной функции в setImmediate или setInterval крашит приложение
    // setImmediate(fn)
    await fn();
    return setInterval(fn, t);
}

async function initUser() {
    function updateUser(newData) {
        $userData.api.replace(newData); // если компонент есть в DOM во время всего приложения, то почему бы и нет. Управлять состоянием здесь, обновлять данные тоже здесь. А в остальных местах - просто useEffect.
    }

    async function fetchUserAndUpdate() {
        try {
            let { user: loggedUser } = await clientHelpers.submitObject(
                '/api/users/getSelf'
            );
            if (!_.isEqual(loggedUser, $userData.getState())) {
                updateUser(loggedUser)
                console.log('user updated after request', loggedUser)
            }
        } catch (e) {
            console.error(e);
        } finally {
        }
    }
    let localUser = JSON.parse(localStorage.getItem('userData') || 'null');
    // console.log('localUser', localUser)
    updateUser(localUser)
    setIntervalAndExecute(fetchUserAndUpdate, 10_000)
}

export default function Component() {

    // обновление глобальных данных приложения. Это все можно делать и в layout где-то, и в отдельном клиентском компоненте где можно юзать useEffect.
    React.useEffect(() => {
        Promise.all([
            initUser()
        ]).then(()=>$isGlobalsLoaded.api.replace(true))
    }, []); // runs once on component mount


    return <></>;
}