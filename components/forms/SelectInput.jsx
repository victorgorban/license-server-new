'use client'
//* секция Библиотеки c функциями
import React from "react";
import AsyncSelect from "react-select/async";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
//* endof  Наши компоненты
export default function SelectInput({ value, ...otherProps }) {

    //* секция глобальное состояние из context
    //* endof глобальное состояние из context


    //* секция состояние

    //* endof состояние


    //* секция вычисляемые переменные, изменение состояния

    //* endof вычисляемые переменные, изменение состояния


    //* секция эффекты

    //* endof эффекты


    //* секция функции-хелперы, НЕ ОБРАБОТЧИКИ

    //* endof функции-хелперы, НЕ ОБРАБОТЧИКИ


    //* секция обработчики
    //* endof обработчики


    return (
        <AsyncSelect
            noOptionsMessage={() => 'Ничего не найдено...'}
            className="w-100"
            classNamePrefix="react-select"
            styles={{ menuPortal: base => ({ ...base, zIndex: 10 }) }}
            // menuPortalTarget={document.body}
            // document.body в модалке плохо работает. Можно не указывать, и поставить menuPosition="fixed", так работает
            menuShouldScrollIntoView={true}
            menuPlacement="auto"
            menuPosition={"fixed"}
            placeholder="Выбрать..."
            value={value}
            {...otherProps}
        />
    )
}