//* секция Библиотеки c функциями
import React from 'react';
import _ from 'lodash';
//* endof  Библиотеки c функциями

//* секция Наши хелперы

//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты
import Button from '@components/animated/Button'
//* endof  Наши компоненты

//* секция Стили компонента
//* endof  Стили компонента

export default React.forwardRef(function Component({ variant="icon", buttonProps = {}, color, iconName, additionalClasses = '', size = 35, ...otherProps }, elRef) {
  let DisplayBlock = variant == 'button' ? Button : 'div';
  buttonProps = variant == 'button' ? buttonProps : {}

  return (
    <DisplayBlock
      ref={elRef}
      className={`d-flex all-center border-${color} color-${color} border-width-1 ${additionalClasses} `}
      style={{ width: size, height: size, borderRadius: "50%" }}
      {...buttonProps}
      {...otherProps}
    >
      {/* тут должна быть иконка инфо */}
      <i className={`fa fa-${iconName}`} style={{ fontSize: 0.857 * size }}></i>
    </DisplayBlock>
  );
})
