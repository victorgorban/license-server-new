'use client'
//* секция Библиотеки c функциями
import React from 'react';
import _ from 'lodash';
import { motion, AnimatePresence } from "framer-motion";
import { usePathname } from 'next/navigation'
//* endof  Библиотеки c функциями

//* секция Наши хелперы
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты

//* endof  Наши компоненты

//* секция Стили компонента

//* endof  Стили компонента

/*
 * Анимация переходов между страницами. Судя по тому что контент изменяется еще до появления анимации, это не то что нужно. К тому же, с моей скоростью обновления страниц, мне ничего не нужно: ни загрузчик, ни анимация переходов.
 */
export default function Component({
    children,
    isAnimated = true
}) {
    const pathname = usePathname();
    const transition = { type: "tween", duration: 0.2 };

    const animations = {
        style: {
            width: '100%',
            height: '100%'
        },
        initial: "in",
        animate: "inactive",
        exit: "out",
        variants: {
            in: {
                y: 50,
                opacity: 0
            },
            inactive: {
                y: 0,
                opacity: 1
            },
            out: {
                y: -50,
                opacity: 0
            },
        },
        transition
    }

    return (
        isAnimated ?
            <AnimatePresence mode="wait">
                <motion.div
                    key={pathname}
                    {...animations}
                    initial="in"
                    animate="inactive"
                    exit="out"
                >
                    {children}
                </motion.div>
            </AnimatePresence>
            : <div className="w-100 h-100"
            >
                {children}
            </div>
    );
}
