export const determineDirection = (first, last) => {
    first = +first;
    last = +last;
    if (first < last) {
        return "inc";
    } else if (first > last) {
        return "dec";
    }

    return "none";
};

const countUp = (val, max) => {
    const numberArray = [];

    for (var i = val; i <= max; i++) {
        numberArray.push(i);
    }

    return numberArray;
};

const countDown = (val, max) => {
    const numberArray = [];

    for (var i = val; i >= max; i--) {
        numberArray.push(i);
    }

    return numberArray;
};

export const difference = (start, end, variant = "long") => {
    let longerArray;
    let shorterArray;
    let isDecreasingInLength;

    const startReversed = [...start].reverse();
    const endReversed = [...end].reverse();

    // Iterate the longer number if there's a difference
    if (startReversed.length > endReversed.length) {
        longerArray = startReversed;
        shorterArray = endReversed;
        isDecreasingInLength = true;
    } else {
        longerArray = endReversed;
        shorterArray = startReversed;
    }

    const numberColumns = longerArray.reduce((acc, item, i) => {
        let arr = [];
        const comparison = shorterArray[i];

        // If the items are the same, there’s been no change in the numbers.
        if (item === comparison) {
            arr = [item];
        } else if (item <= comparison) {
            arr = countDown(comparison, item);
        } else if (item >= comparison) {
            arr = countUp(comparison, item);
        } else if (typeof comparison === "undefined" && !isDecreasingInLength) {
            arr = [item];
        }

        acc.push(arr);

        return acc;
    }, []);

    const numberDiff = numberColumns.reverse();

    //  if (variant == 'short') {
    //     for (let i = 0; i < numberDiff.length; i++) {
    //         let newCol = numberDiff[i].length ? [_.first(numberDiff[i]), _.last(numberDiff[i])] : numberDiff[i]
    //         numberDiff[i] = newCol;
    //     }
    // }

    // If we are descending, reverse each individual column
    if (isDecreasingInLength) {
        return numberDiff.map((col) => col.reverse());
    }

    return numberDiff;
};
