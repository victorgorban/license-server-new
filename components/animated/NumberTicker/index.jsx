'use client'
//* секция Библиотеки c функциями
import React from 'react';
import _ from 'lodash';
import { usePrevious } from "@uidotdev/usehooks";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as localHelpers from './helpers'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты

//* endof  Наши компоненты

//* секция Стили компонента
/* нужно выбирать. Или импортируем переменные из name.module.scss, или сами стили из name.scss. 
 * Стили получаются общими, как бы включаются в общий файл - т.е. стили оттуда можно использовать и в других местах страницы. 
 * После размонтирование компонента стили изчезают.
 * Из тех вариантов что я видел, включение обычного локального файла (не модуля) - самое простое и незамороченное решение, 
 * не требующее доп настройки и тд. Единственное что нужно - чтобы не было конфликтов с более глобальными стилями. 
 * Для этого я добавляю .scoped - и все, понятно что это не глобальный стиль.
*/
import './styles.scss'

//* endof  Стили компонента

/*
 * Скопировано и доработано с поста здесь:
 * https://letsbuildui.dev/articles/building-a-react-number-ticker
 * В оригинале была ошибка в css, пришлось отлаживать и фиксить.
 * Также логика 2х параметров-чисел заменена на 1 число - текущее значение.
 * Добавлены варианты отображения - длинный и короткий.
 * Пофикшено мигание числа из-за лишних рендеров (лишний useEffect).
 */
export default function Component({
    value: currentValue,
    // short, long. long: между, например, 3 и 7 прокручиваются все промежуточные цифры (4,5,6). short: между 3 и 7 промежуточные цифры не прокручиваются.
    variant = "long"
}) {
    currentValue = +currentValue || 0
    const prevValue = usePrevious(currentValue) || 0
    // до hasMounted нужно замерить высоту элемента, чтобы использовать это как окно отображения
    const [hasMounted, setHasMounted] = React.useState(false);
    // готов когда выставлена heightRef.current, а она выставляется при первом рендере
    const [isReadyToAnimate, setReadyToAnimate] = React.useState(false);

    const prevValueArray = Array.from(`${prevValue}`, String);
    const currentValueArray = Array.from(`${currentValue}`, String);

    // const timeoutRef = React.useRef();
    const elementRef = React.useRef();
    // выставляется при первом рендере, чтобы не показывать всю колонку чисел
    const heightRef = React.useRef(20);

    // FIXME внутри вкладки, которая изначально скрыта, heightRef.current равен 0. Наверно потому что display: none. Надо по-другому сделать, я же делал вкладки через framer-motion.
    React.useEffect(() => {
        if (hasMounted) {
            setReadyToAnimate(true);
        } else {
            heightRef.current = elementRef.current.offsetHeight || 20;
            setHasMounted(true);
        }
    }, [currentValue]);

    const diff = localHelpers.difference(prevValueArray, currentValueArray, variant);

    return (
        <span ref={elementRef} className="number">
            {currentValue}
        </span>
    );
}


{/* <span
            // className="animated number-ticker"
            className="scoped__number-ticker"
            style={{ height: `${heightRef.current}px` }}
        >
            {(isReadyToAnimate && prevValue) ? (
                diff?.map((array, i) => {
                    if (!array?.length) {
                        return null;
                    }

                    let direction = localHelpers.determineDirection(
                        array[0],
                        array[array.length - 1]
                    );
                    array = array.map(n => +n)

                    return (
                        <span key={i} className="number item">
                            <span className={`col ${direction}`}>
                                {array?.map((item) => (
                                    <span key={`${item}-${i}`}>{item}</span>
                                ))}
                            </span>
                        </span>
                    );
                })
            ) : (
                <span ref={elementRef} className="number">
                    {currentValue}
                </span>
            )}
        </span> */}