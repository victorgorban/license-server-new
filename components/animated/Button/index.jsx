'use client'
//* секция Библиотеки c функциями
import React from 'react';
import _ from 'lodash';
import { motion, AnimatePresence } from "framer-motion";
//* endof  Библиотеки c функциями

//* секция Наши хелперы
import * as localHelpers from './helpers'
//* endof  Наши хелперы

//* секция Контекст и store
//* endof  Контекст и store

//* секция Компоненты из библиотек
//* endof  Компоненты из библиотек

//* секция Наши компоненты

//* endof  Наши компоненты

//* секция Стили компонента

//* endof  Стили компонента

/*
 * Кнопка. Анимация нужна в первую очередь для изменения размеров во время загрузки.
 */
export default React.forwardRef(function Component({
    variant = "default", // default, green, danger
    isLoading = false,
    className = "",
    children,
    ...otherProps
}, elRef) {
    return (
        
        <motion.button
            ref={elRef}
            type="button"
            className={`btn style-${variant} ${className} ${isLoading && 'loading'}`}
            whileHover={{ scale: 1.06 }}
            whileFocus={{ scale: 1.06 }}
            whileTap={{ scale: 0.96 }}
            // ставлю длину анимации 0, т.к. благодаря моей глобальной установке на анимацию всего, всё все равно анимируется, причем приятно.
            transition={{ duration: 0 }}
            {...otherProps}
        >
            {children}
            <div className="loader lds-ring color-brand1">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </motion.button>
    );
})
